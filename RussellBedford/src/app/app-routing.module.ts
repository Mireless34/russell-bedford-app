import { LoginComponent } from "./login/login.component";
import { ReportesComponent } from "./reportes/reportes.component";
import { ReporteComponent } from "./reportes/reporte.component";
import { Routes } from "@angular/router";
import { PantallaErrorComponent } from './pantallaError/pantallaError.component';
import { PerfilUsuarioComponent } from './PerfilUsuario/perfilUsuario.component';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';

export const routes : Routes = [
    { path: "", component: LoginComponent, pathMatch: 'full' },    
    { path: "reportes", component: ReportesComponent },
    { path: "reporte", component: ReporteComponent },
    { path: "pantallaError", component: PantallaErrorComponent },
    { path: "perfilUsuario", component: PerfilUsuarioComponent },
    { path: "notificaciones", component: NotificacionesComponent }
];

export const navigatableComponents = [    
    LoginComponent,
    ReportesComponent,
    ReporteComponent,
    PantallaErrorComponent,
    PerfilUsuarioComponent,
    NotificacionesComponent
];