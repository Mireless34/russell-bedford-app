import { Component, NgZone, OnDestroy, OnInit, ViewContainerRef } from "@angular/core";
import { IndicadorProcesos } from "~/app/shared/IndicadorProcesos.service";
import * as dialogs from '@nativescript/core/ui/dialogs';
import { Router } from "@angular/router";
import { ModalDialogOptions, ModalDialogService, RouterExtensions } from "@nativescript/angular";
import * as application from '@nativescript/core/application';
import { AndroidApplication, isAndroid } from '@nativescript/core';
import { AndroidActivityBackPressedEventData } from '@nativescript/core';

@Component({
    selector: "notificaciones",
    moduleId: module.id,
    templateUrl: "notificaciones.component.html",
    styleUrls: ["notificaciones.component.css"],
    providers: [IndicadorProcesos]
})

export class NotificacionesComponent implements OnInit, OnDestroy {

    constructor(private routerext: RouterExtensions, private zone: NgZone) {
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                this.regresaAReportes();
            });
        } else {
            return;
        }
     }

    ngOnInit(): void {

    }

    ngOnDestroy(): void {

    }

    regresaAReportes() {
        setTimeout(() => {
            this.zone.run(() => {
                this.routerext.navigate(["./reportes"], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
            });
        }, 200);
    }
}