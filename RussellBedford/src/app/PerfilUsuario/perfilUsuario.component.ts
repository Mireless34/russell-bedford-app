import { Component, NgZone, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { getString, setString } from '@nativescript/core/application-settings';
import { RadDataForm } from 'nativescript-ui-dataform';
import { IndicadorProcesos } from '../shared/indicadorProcesos.service';
import { AndroidApplication, isAndroid } from '@nativescript/core';
import * as dialogs from '@nativescript/core/ui/dialogs';
import { Subscription } from 'rxjs/internal/Subscription';
import { LoginService } from "~/app/shared/login.service";
import { ModalDialogOptions, ModalDialogService } from '@nativescript/angular';
import { ModalPerfil } from '~/app/modales/modal-perfil.component';
import * as application from '@nativescript/core/application';
import { AndroidActivityBackPressedEventData } from '@nativescript/core';
import { exit } from "nativescript-exit";
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
const moment = require("moment");
@Component({
    selector: 'perfilUsuario',
    moduleId: module.id,
    templateUrl: 'perfilUsuario.component.html',
    styleUrls: ['perfilUsuario.component.css'],
    providers: [LoginService, IndicadorProcesos]
})

export class PerfilUsuarioComponent implements OnInit, OnDestroy {

    loginUsuario = JSON.parse(getString("loginUsuario"));
    oDatosUsuario: any;
    oDatosContrasenia: any;
    bContrasenia: boolean = false;
    sInicialesUsuario: string;
    private apiSubscribe: Subscription;
    cancelServicio: any = [];
    bTieneNumero: boolean = false;
    bTieneLongitud: boolean = false;
    bTieneMayusculasMinusculas: boolean = false;
    bTieneCaracterEspecial: boolean = false
    bMostrarAlertas: boolean = false;

    constructor(private zone: NgZone, private routerext: RouterExtensions, private indicadorProceso: IndicadorProcesos, private loginService: LoginService,
        private viewContainerRef: ViewContainerRef, private modalService: ModalDialogService) {
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                this.regresaAReportes();
            });
        } else {
            // this.esAndroid = false;
            return;
        }
    }

    ngOnInit() {
        this.sInicialesUsuario = (this.loginUsuario.nom_empleado).toUpperCase().substring(0, 1) + (this.loginUsuario.apellido).toUpperCase().substring(0, 1);
        this.oDatosUsuario = {
            noEmpleado: this.loginUsuario.num_empleado,
            nombre: this.loginUsuario.NombreCompleto,
            area: this.loginUsuario.nom_departamento,
            correo: this.loginUsuario.email
        };
        this.oDatosContrasenia = {
            contrasenia: "",
            confirmaContrasenia: ""
        };
        setTimeout(() => {
            const sideDrawer = <RadSideDrawer>application.getRootView();
            sideDrawer.gesturesEnabled = false;
        }, 500);
    }

    ngOnDestroy(): void {
        if (this.cancelServicio.length > 0) {
            this.cancelServicio.forEach(c => {
                if (c) {
                    c.unsubscribe();
                }
            });
        }
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                dialogs.confirm({
                    title: "Cerrar Reporte de tiempo",
                    message: "¿Desea cerrar la aplicación?",
                    okButtonText: "Si",
                    cancelButtonText: "No",
                    cancelable: false
                }).then(result => {
                    if (result) {
                        exit();
                    }
                });
            });
        } else {
            return;
        }
    }

    onDataFormLoadedAsegurado(event) {
        const dataForm = event.object;
        if (dataForm._ios && dataForm._nativeDelegate && !dataForm._ios.delegate) {
            dataForm._ios.delegate = dataForm._nativeDelegate;
        }
        dataForm.getPropertyByName("noEmpleado").readOnly = true;
        dataForm.getPropertyByName("nombre").readOnly = true;
        dataForm.getPropertyByName("area").readOnly = true;
        dataForm.getPropertyByName("correo").readOnly = true;
    }

    onDataFormLoadedContrasenia(event) {
        const dataForm = event.object;
        if (dataForm._ios && dataForm._nativeDelegate && !dataForm._ios.delegate) {
            dataForm._ios.delegate = dataForm._nativeDelegate;
        }
    }

    onPropertyValidatedContrasenia(args) {
        const dataForm = <RadDataForm>args.object;
        const contrasenia = dataForm.getPropertyByName("contrasenia");
        const confirmaContrasenia = dataForm.getPropertyByName("confirmaContrasenia");
        setTimeout(() => {
            this.bContrasenia = false;
            if (contrasenia.valueCandidate !== "" && confirmaContrasenia.valueCandidate === '') {
                this.bContrasenia = false;
            } else if (contrasenia.valueCandidate !== confirmaContrasenia.valueCandidate) {
                confirmaContrasenia.errorMessage = 'La contraseña no coincide';
                dataForm.notifyValidated('confirmaContrasenia', false);
                this.bContrasenia = false;
            } else if (contrasenia.valueCandidate.trim() && confirmaContrasenia.valueCandidate.trim()) {
                dataForm.notifyValidated('confirmaContrasenia', true);
                this.bContrasenia = true;
                this.checkPasswordStrength();
            }
        });
    }

    /*****************
     * IMAGEN BASE 64
     ****************/

    abrirFotoPerfil() {
        const options: ModalDialogOptions = {
            animated: true,
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {
                ruta: "",
                imagenBase64: this.loginUsuario.ProfilePicture,
            },
            cancelable: true,
            stretched: false
        };
        this.modalService.showModal(ModalPerfil, options)
            .then((dialogResult: string) => {
                this.loginUsuario.ProfilePicture = dialogResult;
            });
    }

    checkPasswordStrength() {
        console.log("Valido password");
        var strength = 0;
        var tips = "";
        let password = this.oDatosContrasenia.contrasenia;
        this.bTieneLongitud = false;
        this.bTieneNumero = false;
        this.bTieneMayusculasMinusculas = false;
        this.bMostrarAlertas = true;
        // Check password length
        if (password.length < 6) {
          tips += "\nUna longitud de por lo menos 6 caracteres. ";
        } else {
          strength += 1;
          this.bTieneLongitud = true;
        }
        // Check for mixed case
        if (password.match(/[a-z]/) && password.match(/[A-Z]/)) {
          strength += 1;
          this.bTieneMayusculasMinusculas = true;
        } else {
          tips += "\nIncluir una letra mayùscula y una minúscula. ";
        }
        // Check for numbers
        if (password.match(/\d/)) {
          strength += 1;
          this.bTieneNumero = true;
        } else {
          tips += "\nIncluir un numero. ";
        }
        // Check for special characters
        // if (password.match(/[^a-zA-Z\d]/)) {
        //   strength += 1;
        // } else {
        //   tips += "Incluir un caracter especial. ";
        // }
        // Return results
        console.log("strength: ", strength);
        if (strength < 1) {
            console.log("Easy to guess. " + tips);
        //   return "Easy to guess. " + tips;
        } else if (strength === 1) {
            console.log("Medium difficulty. " + tips);
        //   return "Medium difficulty. " + tips;
        } else if (strength === 2) {
            console.log("Difficult. " + tips);
        //   return "Difficult. " + tips;
        } else {
            console.log("Extremely difficult. " + tips);
        //   return "Extremely difficult. " + tips;
        }
        if (strength < 3) {
            dialogs.alert({
                title: 'Contraseña',
                message: `La contraseña debe contener lo siguinete;\n${tips}\nFavor de verificarlo`,
                okButtonText: 'Aceptar',
                cancelable: false
            })
        } else {

        }
      }

    actualizaContrasenia() {
        this.indicadorProceso.show("Actualizando contraseña");
        this.apiSubscribe = this.loginService.actualizaContrasenia(this.loginUsuario.id_empleado, this.oDatosContrasenia.contrasenia)
            .subscribe({
                next: (oRespuesta) => {
                    let respuesta = JSON.parse(oRespuesta);
                    if (respuesta.Correct) {
                        setTimeout(() => {
                            setString("contrasena", this.oDatosContrasenia.contrasenia);
                            dialogs.alert({
                                title: "Reporte de tiempo",
                                message: "Contraseña actualizada correctamente",
                                okButtonText: "Aceptar",
                                cancelable: false
                            });
                            this.oDatosContrasenia = {
                                contrasenia: "",
                                confirmaContrasenia: ""
                            };
                        }, 50);
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.alert({
                        title: "Reporte de tiempo",
                        message: "Ocurrio un error al intentar actualizar la contraseña, favor de intentarlo más tarde.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
        this.cancelServicio.push(this.apiSubscribe);
    }

    regresaAReportes() {
        setTimeout(() => {
            this.zone.run(() => {
                this.routerext.navigate(["./reportes"], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
            });
        }, 200);
    }
}