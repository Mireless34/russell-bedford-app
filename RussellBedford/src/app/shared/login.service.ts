import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap, catchError } from "rxjs/operators";
import { ConfiguraAPI } from './configuraAPI';

@Injectable()
export class LoginService {

    constructor(private http: HttpClient) { }

    //     SELECT
//     empleado.id_empleado,
//     empleado.num_empleado,
// 		empleado.nom_empleado,
// 		empleado.apellido,
//     CONCAT (nom_empleado, ' ', apellido) as 'NombreCompleto',
//     empleado.email,
//     nivel.id_nivel,
//     departamento.id_departamento,
//     departamento.nom_departamento,
// 		empleado.estado,
// 		empleado.estatus_tiempo,
//     nivel.clave_nivel,
//     cuota.cuota,  
//     cuota.costo
// FROM
//     empleado
//     INNER JOIN nivel ON nivel.id_nivel = empleado.nivel
//     INNER JOIN departamento ON departamento.id_departamento= empleado.num_departamento
//     LEFT OUTER JOIN cuota ON departamento.id_departamento = cuota.area AND nivel.id_nivel = cuota.nivel
// WHERE empleado.num_empleado =
    validaAcceso(usuario: string, contrasenia: string) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}empleado/login`,
            {
                num_empleado: usuario,
                pwd: contrasenia
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    actualizaFotoPerfil(usuario: string, imagenBase64: any) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}empleado/ImgInsertOrUpdate`,
            {
                id_empleado: usuario,
                ProfilePicture: imagenBase64
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    eliminaFotoPerfil(usuario: string) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}empleado/ImgDelete`,
            {
                id_empleado: usuario
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    actualizaContrasenia(idEmpleado: number, contrasenia: string) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}empleado/ActualizaContrasena`,
            {
                num_empleado: idEmpleado,          
                pwd: contrasenia
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    validaEmail(correoElectronico: string) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}empleado/ExisteCorreo`,
            {
                Email: correoElectronico,
                
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    enviarCorreoElectronico(correoElectronico: string, para: string, asunto: string, cuerpo: string, copia: string, copiaOculta: string) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}empleado/`,
            {
                De: "informes@gmail.com.mx",
                Para: para,
                Titulo: asunto,
                Html: true,
                Cuerpo: cuerpo,
                Prioridad: 0,
                Copia: copia,
                Oculta: copiaOculta,
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    setTokenDispositivoNotificacionesPush(token: string, idDispositivo: string, idEmpleado: number, plataforma: string, version: string, manufactura: string) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}dispositivo/registrar`, //Cambiar Nombre 'Agregar dispositivo`
            {
                Token: token,
                IdDispositivo: idDispositivo,
                IdEmpleado: idEmpleado,
                SistemaOperativo: plataforma,
                VersionSO: version,
                Manufactura: manufactura
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }
}