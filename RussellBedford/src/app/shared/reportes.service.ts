import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { catchError, map } from "rxjs/operators";
import { ConfiguraAPI } from "./configuraAPI";

@Injectable({ providedIn: 'root' })

export class ReportesService {

    constructor(private http: HttpClient) { }

    getReportes(sNumEmpleado: string) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}empleado/reportes`,
            {
                idEmpleado: sNumEmpleado
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    getReporte(nIdReporte: number) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}tiempo/reportePorEmpleado`,
            {
                idTiempo: nIdReporte
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    getReporteHorasAsignables(nIdReporte: number) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}reportes/reporte`,
            {
                id_tiempo: nIdReporte
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    getReporteHorasNoAsignables(nIdReporte: number) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}reportes/horasNoAsignables`,
            {
                id_tiempo: nIdReporte
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    /***********************
     * HORAS ASIGNABLES
     **********************/
    //#region 
    getGerentesConProyecto() {
        return this.http.get(
            `${ConfiguraAPI.API_URL}cliente/gerentesConProyecto`,
            {
                headers: ConfiguraAPI.getCommonHeaders()
            },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    getClientesPorGerente(nIdGerente: number) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}cliente/clientesPorGerente`,
            {
                id_Gerente: nIdGerente
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    getProyectosPorGerenteCliente(nIdGerente: number, nCliente: number) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}cliente/proyectosPorClienteGerente`,
            {
                id_gerente: nIdGerente,
                id_cliente: nCliente
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }
    //#endregion

    /***********************
     * HORAS NO ASIGNABLES
     **********************/

    //#region 
    getActividadesPorEmpleado(nIdEmpleado: any) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}actividad/actividadesPorEmpleado`,
            {
                IdEmpleado: nIdEmpleado
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    deleteHoraNoAsignable(idHoraNoAsignable: number, horasNoAsignables: number, totalHoras: number, idTiempo: number) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}tiempo/EliminaHoraNoAsignable`,
            {
                IdNoAsignable: idHoraNoAsignable,
                resta_no_asignable: horasNoAsignables,
                resta_total: totalHoras,
                idTiempo: idTiempo
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    validaHorasPorActividad(actividad: number, idEmpleado: number) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}tiempo/EliminaHoraNoAsignable`,
            {
                actididad: actividad,
                empleado: idEmpleado,
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }
    //#endregion
    getMeses() {
        return this.http.get(
            `${ConfiguraAPI.API_URL}periodo/meses`,
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    getAnios() {
        return this.http.get(
            `${ConfiguraAPI.API_URL}periodo/anios`,
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    getValidaPeriodo(quincena: string, mes: string, anio: number, idEmpleado) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}periodo/existePeriodo`,
            {
                quincena: quincena,
                mes: mes,
                anio: anio,
                empleado: idEmpleado
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    saveReporteDeTiempo(quincena: number, mes: string, anio: number, totalAsignables: number, totalNoAsignables: number, horasTotales: number, iDEmpleado: number, estatus: number, envio: number, fechaReporte: string, detalleAsignables: any, detalleNoAsignables: any) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}periodo/registraNuevoReporte`,
            {
                id_tiempo: 32777,
                quincena: quincena,
                mes: mes,
                anio: anio,
                totalhorasasignables: totalAsignables,
                totalhorasnoasignables: totalNoAsignables,
                total: horasTotales,
                idempleado: iDEmpleado,
                estatus: estatus,
                envio: envio,
                fechareporte: fechaReporte,
                detalleHorasAsignables: detalleAsignables,
                detalleHorasNoAsignables: detalleNoAsignables
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    deleteReporteDeTiempo(idTiempo: number) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}periodo/EliminarReporte`,
            {
                idTiempo: idTiempo,
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }

    enviaCorreo(para: string, titulo: string, cuerpo: string, copia: string, copiaOculta: string) {
        return this.http.post(
            `${ConfiguraAPI.API_URL}correo/enviar_correo`,
            {
                Para: para,
                Titulo: titulo,
                Cuerpo: cuerpo,
                Copia: copia,
                Oculta: copiaOculta
            },
            { headers: ConfiguraAPI.getCommonHeaders() },
        )
            .pipe(
                map(response => JSON.stringify(response)),
                catchError(ConfiguraAPI.handleErrors)
            )
    }
}