import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { getString } from '@klippa/nativescript-http';
import { throwError } from 'rxjs';

@Injectable()
export class ConfiguraAPI {
    public static API_URL: string = "http://192.168.3.59/stuxnet/api/";
    
    public static getCommonHeaders() {
        let headers = new HttpHeaders();
        headers.append("Content-Type", "application/json");
        return headers;
    }

    public static handleErrors(error: HttpErrorResponse) {
        console.log(JSON.stringify(error));
        return throwError(() => error);
    }
}