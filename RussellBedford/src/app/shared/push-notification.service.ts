import { Injectable, NgZone } from '@angular/core';
import { ApplicationSettings, isAndroid } from '@nativescript/core';
import { firebase } from '@nativescript/firebase-core';
import * as dialogs from '@nativescript/core/ui/dialogs';
import { IndicadorProcesos } from './indicadorProcesos.service';
import '@nativescript/firebase-messaging';
import { ReportesService } from './reportes.service';
import { RouterExtensions } from '@nativescript/angular';
import { setString } from '@nativescript/core/application-settings';

@Injectable({
    providedIn: 'root',
})
export class PushNotificationService {

    mensajeModal: any;
    archivosModal: any;

    constructor(private zone: NgZone, private indicadorProceso: IndicadorProcesos, private reportesService: ReportesService, private routerext: RouterExtensions) {
        const messaging = firebase().messaging();
        messaging.onMessage((message) => {
            this.onMessageReceived(message);
        });

        messaging.onNotificationTap((message) => {
            this.verNotificacion(message);
        });
    }

    onMessageReceived(message: any) {
        setTimeout(() => {
            let promocionesPush: any = ApplicationSettings.getString("promocionesNotif") ? ApplicationSettings.getString("promocionesNotif") : false;
            if (promocionesPush) {
                promocionesPush = JSON.parse(promocionesPush);
            } else {
                promocionesPush = [];
            }
            let existePromocion = Boolean(message.data.promocion);
            if (existePromocion) {
                promocionesPush.push(JSON.parse(message.data.promocion));
                ApplicationSettings.setString("promocionesNotif", JSON.stringify(promocionesPush));
                this.zone.run(() => {
                    // this.popoverService.showSnackbar("Nueva Promoción");
                });
            } else {
                this.zone.run(() => {
                    // this.popoverService.showSnackbar("Nueva Notificación");
                });
            }
        }, 500);
    }

    verNotificacion(message: any) {
        let existeNotificacion = Boolean(message.data.dataNotificacion);
        console.log('Mensaje de notificación:',existeNotificacion);
        if (existeNotificacion) {
            let notificacion = JSON.parse(message.data.dataNotificacion);
            console.log('notificacion: ', notificacion.periodo);
            if (isAndroid) {
                setTimeout(() => {
                    ApplicationSettings.setBoolean("notificacionPush", true);
                    ApplicationSettings.setString("notificacion", JSON.stringify(notificacion.periodo));
                    setString("oPeriodoTiempo", JSON.stringify(notificacion.periodo));
                }, 500);
            } else {
                setTimeout(() => {
                    this.zone.run(() => {
                        let notificacion = JSON.parse(message.data.dataNotificacion);
                        console.log('Periodo: ', notificacion.periodo);
                        let idTiempo = notificacion.periodo.IdTiempo;
                            this.indicadorProceso.show("Abriendo notificación...");
                            this.reportesService.getReporte(idTiempo)
                                .subscribe({
                                    next: (data) => {
                                        let reporte = JSON.parse(data);
                                        if (reporte) {
                                            setTimeout(() => {
                                                this.indicadorProceso.hide();
                                                setString("oPeriodoTiempo", (JSON.stringify(notificacion.periodo)));
                                                this.zone.run(() => {
                                                    this.routerext.navigate([`./${'reporte'}`], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
                                                });
                                            }, 500);
                                        } else {
                                            dialogs.alert({
                                                title: 'Notificación',
                                                message: 'El reporte de tiempo ya se encuentra cerrado.',
                                                okButtonText: 'Aceptar',
                                                cancelable: false
                                            })
                                        }
                                    },
                                    error: (error) => {
                                        this.indicadorProceso.hide();
                                        dialogs.alert({
                                            title: 'Notificación',
                                            message: 'Ocurrio un error al abrir la notificación',
                                            okButtonText: 'Aceptar',
                                            cancelable: false
                                        })
                                    },
                                    complete: () => {
                            //             this.dashboardService.setLecturaNotificacion(String(notificacion.IdBit), bclick)
                            //             .subscribe({
                            //                 next: (data) => {
                            //                 },
                            //                 error: (error) => {
                            //                     this.indicadorProceso.hide();
                            //                     this.popoverService.showAlert({
                            //                         titulo: "Notificación",
                            //                         mensaje: "No se pudo marcar como leida esta notificación.",
                            //                         textoBoton: "Aceptar"
                            //                     });
                            //                 },
                            //                 complete: () => {
                            //                     this.indicadorProceso.hide();
                            //                     this.popoverService.showModalPopoverBottomSheet({
                            //                         titulo: notificacion.Titulo,
                            //                         mensaje: this.mensajeModal[0].Comentario,
                            //                         fechaHora: notificacion.Fecha + " " + notificacion.Hora,
                            //                         archivos: this.mensajeModal[0] ? this.mensajeModal[0]: null,
                            //                         idT: notificacion.IdT,
                            //                         nomComponente: notificacion.Tipo,
                            //                         componenteRouting: sComponenteRouting
                            //                     }, 'notificaciones');
                            //                 }
                            //             });
                                    }
                                });
                    });
                }, 1000);
            }
        }
    }
}
