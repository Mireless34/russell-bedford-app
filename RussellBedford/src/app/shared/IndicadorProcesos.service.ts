import { Injectable } from "@angular/core";
import { LoadingIndicator, Mode, OptionsCommon } from "@nstudio/nativescript-loading-indicator";

@Injectable({ providedIn: 'root' })
export class IndicadorProcesos {
    private indicator: LoadingIndicator;

    constructor() {
        this.indicator = new LoadingIndicator();
    }

    public show(message?: string, options?: any) {
        const defaultOptions: OptionsCommon = {
            message: message,
            dimBackground: true,
            color: '#000000',
            margin: 10,
            userInteractionEnabled: false,
            mode: Mode.Indeterminate,
            android: {
                cancelable: false,
            },
            ios: {
                square: false,
            },
        };
        this.indicator.show({
            ...defaultOptions,
            ...options,
        });
    }

    public hide() {
        this.indicator.hide();
    }
}
