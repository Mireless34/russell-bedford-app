import { NgModule, NO_ERRORS_SCHEMA, LOCALE_ID, APP_INITIALIZER } from "@angular/core";
import { NativeScriptModule, NativeScriptRouterModule, NativeScriptFormsModule, ModalDialogService } from "@nativescript/angular";
import { NativeScriptHttpClientModule } from "@klippa/nativescript-http/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";
import { routes, navigatableComponents } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NativeScriptMaterialCardViewModule } from "@nativescript-community/ui-material-cardview/angular";
import { NativeScriptMaterialTabsModule } from "@nativescript-community/ui-material-tabs/angular";
import { PreloadAllModules } from "@angular/router";
import { modalPeriodoComponent } from "./modales/modal-periodo.component";
import { modalHorasComponent } from "./modales/modal-horas.component";
import { ModalPerfil } from './modales/modal-perfil.component';
import { modalUniversalComponent } from './modales/modal-universal.component';
import { PushNotificationService } from './shared/push-notification.service';
@NgModule({
    providers: [ModalDialogService,
        PushNotificationService,
        { provide: LOCALE_ID, useValue: 'es-MX' }
    ],
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        NativeScriptMaterialCardViewModule,
        NativeScriptMaterialTabsModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIListViewModule,
        NativeScriptUIDataFormModule,
        NativeScriptHttpClientModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    declarations: [
        AppComponent,
        modalPeriodoComponent,
        modalHorasComponent,
        ModalPerfil,
        modalUniversalComponent,
        ...navigatableComponents
    ],
    entryComponents: [
        modalPeriodoComponent,
        modalHorasComponent,
        ModalPerfil,
        modalUniversalComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
