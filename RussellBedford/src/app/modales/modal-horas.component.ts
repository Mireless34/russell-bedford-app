import { Component, OnInit, ViewChild, ElementRef, OnDestroy, NgZone } from "@angular/core";
import { ModalDialogParams, RouterExtensions } from "@nativescript/angular";
import { AndroidApplication, isAndroid } from '@nativescript/core';
import * as dialogs from '@nativescript/core/ui/dialogs';
import { View } from "@nativescript/core";
import { Observable } from '@nativescript/core';
import * as application from '@nativescript/core/application';
import { IndicadorProcesos } from "../shared/IndicadorProcesos.service";
import { ReportesService } from "~/app/shared/reportes.service"
import { AndroidActivityBackPressedEventData } from '@nativescript/core';
import { getString } from "@nativescript/core/application-settings";
import { Subscription } from "rxjs";

const moment = require("moment");
moment.updateLocale('es', { parentLocale: 'es' });
@Component({
    selector: "horas",
    moduleId: module.id,
    templateUrl: "modal-horas.component.html",
    styleUrls: ["modal-universal.css"],
    providers: [ReportesService, IndicadorProcesos]
})

export class modalHorasComponent extends Observable implements OnInit, OnDestroy {

    loginUsuario = JSON.parse(getString("loginUsuario"));
    nQuincena: number;
    sMes: string;
    sTipoHoras: string;
    oDatosGteCte: any;
    aGerentes: any;
    aListaGerente: any;
    nIdGerente: number;
    aClientes: any;
    aListaClientes: any;
    nIdCliente: number;
    aProyectos: any;
    aListaProyectos: any;
    nIdProyecto: number;
    oDatosHorasAsignables: any = {};
    //ACTIVIDADES
    aListaActividades: any;
    aActividades: any;
    nIdActividad: number = 0;
    sDescripcion: string;
    oDatosHorasNoAsignables: any;
    //TODO: HORAS ASIGNABLES
    oDatosActividades: any;
    nUno: number;
    nDos: number;
    nTres: number;
    nCuatro: number;
    nCinco: number;
    nSeis: number;
    nSiete: number;
    nOcho: number;
    nNueve: number;
    nDiez: number;
    nOnce: number;
    nDoce: number;
    nTrece: number;
    nCatorce: number;
    nQuince: number;
    nDiez_y_seis: number;
    nTotal: number = 0;
    oPropiedadesNumeros: any = {};
    asignablesNoAsignables: any = [];
    proyectoActividad: boolean = false;
    private apiSubscribe: Subscription;
    cancelServicio: any = [];
    tituloModal: string = "";

    @ViewChild('modal', { static: false }) modal: ElementRef<View>;

    constructor(private params: ModalDialogParams, private reportesService: ReportesService, private indicadorProceso: IndicadorProcesos,
        private zone: NgZone, private routerext: RouterExtensions) {
        super();
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                this.cerrarModal();
            });
        } else {
            // this.esAndroid = false;
            return;
        }
    }

    ngOnInit() {
        this.nQuincena = Number(this.params.context.reporte.quincena);
        this.sTipoHoras = this.params.context.tipoHoras;
        this.asignablesNoAsignables = this.params.context.asignablesNoAsignables;
        this.sMes = this.params.context.reporte.mes;
        this.tituloModal = this.sTipoHoras === 'asignables' ? 'Horas asignables' : 'Horas no asignables';
        setTimeout(() => {
            if (this.sTipoHoras === "asignables") {
                this.oDatosHorasAsignables = this.params.context.oAsignables;
                this.recuperaGerentes();
                if (this.oDatosHorasAsignables.Clave) {
                    this.recuperaHorasAsignables();
                } else {
                    this.oDatosGteCte = {
                        gerente: "Seleccione una opción",
                        cliente: "Seleccione una opción",
                        proyecto: "Seleccione una opción"
                    };
                }
            } else if (this.sTipoHoras === "noAsignables") {
                this.recuperaActividades(this.loginUsuario.id_empleado);
                this.oDatosHorasNoAsignables = this.params.context.oAsignables;
                if (this.oDatosHorasNoAsignables.NomActividad) {
                    this.recuperaHorasNoAsignables();
                } else {
                    this.oDatosActividades = {
                        actividad: "Seleccione una opción"
                    };
                }
            }
        });
        this.validaFinDeSemana(this.nQuincena, ((moment().month(this.sMes).format("M")).length === 1 ? ("0" + moment().month(this.sMes).format("M")) : moment().month(this.sMes).format("M")), this.params.context.reporte.ano);
    }

    ngOnDestroy(): void {
        if (this.cancelServicio.length > 0) {
            this.cancelServicio.forEach(c => {
                c.unsubscribe();
            });
        }
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                dialogs.confirm({
                    title: "Reporte de tiempo",
                    message: "¿Desea salir sin enviar o guardar su reporte de tiempo?",
                    okButtonText: "Si",
                    cancelButtonText: "No",
                    cancelable: false
                }).then(resultado => {
                    if (resultado) {
                        setTimeout(() => {
                            this.zone.run(() => {
                                this.routerext.navigate(["/reportes"], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
                            });
                        }, 300);
                    }
                })
            });
        } else {
            return;
        }
    }

    sumaHoras() {
        setTimeout(() => {
            this.nTotal =
                Number(this.nUno || this.nUno !== undefined ? this.nUno : 0) +
                Number(this.nDos || this.nDos !== undefined ? this.nDos : 0) +
                Number(this.nTres || this.nTres !== undefined ? this.nTres : 0) +
                Number(this.nCuatro || this.nCuatro !== undefined ? this.nCuatro : 0) +
                Number(this.nCinco || this.nCinco !== undefined ? this.nCinco : 0) +
                Number(this.nSeis || this.nSeis !== undefined ? this.nSeis : 0) +
                Number(this.nSiete || this.nSiete !== undefined ? this.nSiete : 0) +
                Number(this.nOcho || this.nOcho !== undefined ? this.nOcho : 0) +
                Number(this.nNueve || this.nNueve !== undefined ? this.nNueve : 0) +
                Number(this.nDiez || this.nDiez !== undefined ? this.nDiez : 0) +
                Number(this.nOnce || this.nOnce !== undefined ? this.nOnce : 0) +
                Number(this.nDoce || this.nDoce !== undefined ? this.nDoce : 0) +
                Number(this.nTrece || this.nTrece !== undefined ? this.nTrece : 0) +
                Number(this.nCatorce || this.nCatorce !== undefined ? this.nCatorce : 0) +
                Number(this.nQuince || this.nQuince !== undefined ? this.nQuince : 0) +
                Number(this.nDiez_y_seis || this.nDiez_y_seis !== undefined ? this.nDiez_y_seis : 0)
        }, 200);
    }

    recuperaGerentes() {
        setTimeout(() => {
            this.indicadorProceso.show("Cargando gerentes...", {
                android: {
                    view: this.modal.nativeElement.android
                }
            });            
        }, 50);
        this.apiSubscribe = this.reportesService.getGerentesConProyecto()
            .subscribe({
                next: (aRespuestaGerentes) => {
                    let listaGerentes = JSON.parse(aRespuestaGerentes);
                    this.aListaGerente = [];
                    this.aGerentes = [];
                    if (listaGerentes.Correct) {
                        this.aListaGerente = listaGerentes.Objects;
                        this.aGerentes.push("Seleccione una opción");
                        this.aListaGerente.forEach(elemento => {
                            this.aGerentes.push(elemento.Nombre);
                        });
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.confirm({
                        title: "Reporte de tiempo",
                        message: "Ocurrio un error al cargar la lista de gerentes, inténtelo nuevamente.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    }).then(result => {
                        if (result) {
                            this.cerrarModal();
                        }
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
            this.cancelServicio.push(this.apiSubscribe);
    }

    recuperaClientes(idGerente: number) {
        setTimeout(() => {
            this.indicadorProceso.show("Cargando clientes...", {
                android: {
                    view: this.modal.nativeElement.android
                }
            });            
        }, 50);
        this.apiSubscribe = this.reportesService.getClientesPorGerente(idGerente)
            .subscribe({
                next: (respuestaCliente) => {
                    let listaClientes = JSON.parse(respuestaCliente);
                    if (listaClientes.Correct) {
                        this.aListaClientes = [];
                        this.aListaClientes = listaClientes.Objects;
                        if (this.aListaClientes) {
                            this.aClientes = [];
                            this.aClientes.push("Seleccione una opción");
                            this.aListaClientes.forEach(elemento => {
                                this.aClientes.push(elemento.Cliente);
                            });
                        } else {
                            dialogs.alert({
                                title: "Reporte de tiempo",
                                message: "El gerente seleccionado no tiene clientes activos actualmente, favor de verificarlo con su gerente.",
                                okButtonText: "Aceptar",
                                cancelable: false
                            });
                        }
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.confirm({
                        title: "Reporte de tiempo",
                        message: "Ocurrio un error al cargar la lista de clientes.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    }).then(result => {
                        if (result) {
                            this.cerrarModal();
                        }
                    });
                },
                complete: () => {
                    setTimeout(() => {
                        this.indicadorProceso.hide();
                    }, 50);
                }
            });
            this.cancelServicio.push(this.apiSubscribe);
    }

    recuperaProyectos(idGerente: number, idCliente: number) {
        setTimeout(() => {
            this.indicadorProceso.show("Cargando proyectos...", {
                android: {
                    view: this.modal.nativeElement.android
                }
            });            
        }, 50);
        this.apiSubscribe = this.reportesService.getProyectosPorGerenteCliente(idGerente, idCliente)
            .subscribe({
                next: (respuestaProyectos) => {
                    let listaProyectos = JSON.parse(respuestaProyectos);
                    this.aListaProyectos = [];
                    if (listaProyectos.Correct) {
                        this.aListaProyectos = listaProyectos.Objects;
                        if (this.aListaProyectos) {
                            this.aProyectos = [];
                            this.aProyectos.push("Seleccione una opción");
                            this.aListaProyectos.forEach(elemento => {
                                this.aProyectos.push(elemento.Nombre);
                            });
                            this.eliminaProyectos();
                        } else {
                            dialogs.alert({
                                title: "Reporte de tiempo",
                                message: "El cliente no tiene proyectos activos actualmente, favor de verificarlo con su gerente.",
                                okButtonText: "Aceptar",
                                cancelable: false
                            });
                        }
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.confirm({
                        title: "Reporte de tiempo",
                        message: "Ocurrio un error al cargar la lista de proyectos, Inténtelo nuevamente.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    }).then(result => {
                        if (result) {
                            this.cerrarModal();
                        }
                    });
                },
                complete: () => {
                    setTimeout(() => {
                        this.indicadorProceso.hide();
                    }, 50);
                }
            });
            this.cancelServicio.push(this.apiSubscribe);
    }

    eliminaProyectos() {
        this.asignablesNoAsignables.forEach(elemento => {
            let indice = 0;
            indice = elemento.Clave !== this.oDatosHorasAsignables.Clave ? this.aProyectos.indexOf(elemento.Clave) : 0;
            if (indice>=1) {
                this.aProyectos.splice(indice, 1);
            }
        });
    }

    recuperaHorasAsignables() {
        this.nIdGerente = this.oDatosHorasAsignables.idGerente;
        this.nIdCliente = this.oDatosHorasAsignables.idCliente;
        this.nIdProyecto = this.oDatosHorasAsignables.IDProyecto;
        this.nUno = this.oDatosHorasAsignables.uno;
        this.nDos = this.oDatosHorasAsignables.dos;
        this.nTres = this.oDatosHorasAsignables.tres;
        this.nCuatro = this.oDatosHorasAsignables.cuatro;
        this.nCinco = this.oDatosHorasAsignables.cinco;
        this.nSeis = this.oDatosHorasAsignables.seis;
        this.nSiete = this.oDatosHorasAsignables.siete;
        this.nOcho = this.oDatosHorasAsignables.ocho;
        this.nNueve = this.oDatosHorasAsignables.nueve;
        this.nDiez = this.oDatosHorasAsignables.diez;
        this.nOnce = this.oDatosHorasAsignables.once;
        this.nDoce = this.oDatosHorasAsignables.doce;
        this.nTrece = this.oDatosHorasAsignables.trece;
        this.nCatorce = this.oDatosHorasAsignables.catorce;
        this.nQuince = this.oDatosHorasAsignables.quince;
        this.nDiez_y_seis = this.oDatosHorasAsignables.diezyseis;
        this.proyectoActividad = true;
        this.recuperaClientes(this.nIdGerente);
        this.recuperaProyectos(this.nIdGerente, this.nIdCliente);
        setTimeout(() => {
            this.oDatosGteCte = {
                gerente: this.oDatosHorasAsignables.Gerente,
                cliente: this.oDatosHorasAsignables.Cliente,
                proyecto: this.oDatosHorasAsignables.Clave
            };
        }, 200);
    }

    /***********
     * RADDATAFORM
     ***********/

    onDataFormLoadedGteCtePo(event) {
        const dataForm = event.object;
        if (dataForm._ios && dataForm._nativeDelegate && !dataForm._ios.delegate) {
            dataForm._ios.delegate = dataForm._nativeDelegate;
        }
    }

    onPropertyValidatedGteCtePo(args) {
        const dataform = args.object;
        const gerente = dataform.getPropertyByName("gerente");
        const cliente = dataform.getPropertyByName("cliente");
        const proyecto = dataform.getPropertyByName("proyecto");
        this.proyectoActividad = false;
        if (args.propertyName === "gerente") {
            if (gerente.valueCandidate !== "Seleccione una opción") {
                this.nIdGerente = this.aListaGerente.filter(v => { return v.Nombre === gerente.valueCandidate })[0].ID;
                this.recuperaClientes(this.nIdGerente);
                this.cambiaDatosGteCte("gerente", gerente.valueCandidate, cliente.valueCandidate, proyecto.valueCandidate);
            } else {
                this.cambiaDatosGteCte("gerente", gerente.valueCandidate, cliente.valueCandidate, proyecto.valueCandidate);
            }
        } else if (args.propertyName === "cliente") {
            if (cliente.valueCandidate !== "Seleccione una opción") {
                this.nIdCliente = this.aListaClientes.filter(v => { return v.Cliente === cliente.valueCandidate })[0].ID;
                this.recuperaProyectos(this.nIdGerente, this.nIdCliente);
                this.cambiaDatosGteCte("cliente", gerente.valueCandidate, cliente.valueCandidate, proyecto.valueCandidate);
            } else {
                this.cambiaDatosGteCte("cliente", gerente.valueCandidate, cliente.valueCandidate, proyecto.valueCandidate);
            }
        } else if (args.propertyName === "proyecto") {
            if (proyecto.valueCandidate !== "Seleccione una opción") {
                this.nIdProyecto = this.aListaProyectos.filter(v => { return v.Nombre === proyecto.valueCandidate })[0].ID
                this.cambiaDatosGteCte("proyecto", gerente.valueCandidate, cliente.valueCandidate, proyecto.valueCandidate)
                this.proyectoActividad = true;
            } else {
                this.cambiaDatosGteCte("proyecto", gerente.valueCandidate, cliente.valueCandidate, proyecto.valueCandidate)
            }
        }
        // dataform.reload();
    }

    cambiaDatosGteCte(dato: string, gerente: string, cliente: string, proyecto: string) {
        switch (dato) {
            case "gerente":
                this.aClientes = [];
                this.aProyectos = [];
                this.oDatosGteCte = {
                    gerente: gerente,
                    cliente: "Seleccione una opción",
                    proyecto: ""
                };
                this.set("formgtecte", this.cambiaDatosGteCte);
                break;
            case "cliente":
                this.aProyectos = [];
                this.oDatosGteCte = {
                    gerente: gerente,
                    cliente: cliente,
                    proyecto: ""
                };
                this.set("formgtecte", this.cambiaDatosGteCte);
                break;
            case "proyecto":
                this.oDatosGteCte = {
                    gerente: gerente,
                    cliente: cliente,
                    proyecto: proyecto
                };
                this.set("formgtecte", this.cambiaDatosGteCte);
                break;
        }
    }

    //Horas No Asignables
    recuperaActividades(nIdEmpleado: number) {
        setTimeout(() => {
            this.indicadorProceso.show("Cargando actividades...", {
                android: {
                    view: this.modal.nativeElement.android
                }
            });            
        }, 50); 
        this.apiSubscribe = this.reportesService.getActividadesPorEmpleado(String(nIdEmpleado))
            .subscribe({
                next: (respuestaActividades) => {
                    let actividades = JSON.parse(respuestaActividades);
                    if (actividades.Correct) {
                        this.aListaActividades = [];
                        this.aListaActividades = actividades.Objects;
                        this.aActividades = [];
                        this.aActividades.push("Seleccione una opción");
                        this.aListaActividades.forEach(elemento => {
                            this.aActividades.push(elemento.NomActividad);
                        });
                        this.eliminaActividades();
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.confirm({
                        title: "Reporte de tiempo",
                        message: "No se cargaron las actividades, inténtelo nuevamente",
                        okButtonText: "Aceptar",
                        cancelable: false
                    }).then(result => {
                        if (result) {
                            this.cerrarModal();
                        }
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
            this.cancelServicio.push(this.apiSubscribe);
    }

    eliminaActividades() {
        this.asignablesNoAsignables.forEach(elemento => {
            let indice = 0;
            indice = elemento.NomActividad !== this.oDatosHorasNoAsignables.NomActividad ? this.aActividades.indexOf(elemento.NomActividad) : 0;
            if (indice>=1) {
                this.aActividades.splice(indice, 1);
            }
        });
    }

    recuperaHorasNoAsignables() {
        this.nIdActividad = this.oDatosHorasNoAsignables.IDActividad;
        this.nUno = this.oDatosHorasNoAsignables.uno;
        this.nDos = this.oDatosHorasNoAsignables.dos;
        this.nTres = this.oDatosHorasNoAsignables.tres;
        this.nCuatro = this.oDatosHorasNoAsignables.cuatro;
        this.nCinco = this.oDatosHorasNoAsignables.cinco;
        this.nSeis = this.oDatosHorasNoAsignables.seis;
        this.nSiete = this.oDatosHorasNoAsignables.siete;
        this.nOcho = this.oDatosHorasNoAsignables.ocho;
        this.nNueve = this.oDatosHorasNoAsignables.nueve;
        this.nDiez = this.oDatosHorasNoAsignables.diez;
        this.nOnce = this.oDatosHorasNoAsignables.once;
        this.nDoce = this.oDatosHorasNoAsignables.doce;
        this.nTrece = this.oDatosHorasNoAsignables.trece;
        this.nCatorce = this.oDatosHorasNoAsignables.catorce;
        this.nQuince = this.oDatosHorasNoAsignables.quince;
        this.nDiez_y_seis = this.oDatosHorasNoAsignables.diezyseis;
        this.proyectoActividad = true;
        this.sDescripcion = this.oDatosHorasNoAsignables.descripcion;
        setTimeout(() => {
            this.oDatosActividades = {
                actividad: this.oDatosHorasNoAsignables.NomActividad
            };
        }, 200);
    }
    /***********
     * RADDATAFORM
     ***********/
    onPropertyValidatedActividades(args) {
        const dataform = args.object;
        const actividad = dataform.getPropertyByName("actividad");
        if (args.propertyName === "actividad") {
            if (actividad.valueCandidate !== "Seleccione una opción") {
                this.nIdActividad = this.aListaActividades.filter(v => { return v.NomActividad === actividad.valueCandidate })[0].IdActividad;
                this.sDescripcion = this.aListaActividades.filter(v => { return v.NomActividad === actividad.valueCandidate })[0].Descripcion;
                this.proyectoActividad = true;
            } else {
                this.nIdActividad = 0;
                this.sDescripcion = "";
                this.proyectoActividad = false;
            }
        }
    }

    agregaHoras() {
        if (this.sTipoHoras === "asignables") {
            this.agregahorasAsignables();
        } else {
            this.validaHorasPorActividad();
        }
    }

    agregahorasAsignables() {
        this.oDatosHorasAsignables = {
            "IDHoraAsignable": 0,
            "IDProyecto": this.nIdProyecto,
            "Clave": this.oDatosGteCte.proyecto,
            "uno": this.nUno ? this.nUno : 0,
            "dos": this.nDos ? this.nDos : 0,
            "tres": this.nTres ? this.nTres : 0,
            "cuatro": this.nCuatro ? this.nCuatro : 0,
            "cinco": this.nCinco ? this.nCinco : 0,
            "seis": this.nSeis ? this.nSeis : 0,
            "siete": this.nSiete ? this.nSiete : 0,
            "ocho": this.nOcho ? this.nOcho : 0,
            "nueve": this.nNueve ? this.nNueve : 0,
            "diez": this.nDiez ? this.nDiez : 0,
            "once": this.nOnce ? this.nOnce : 0,
            "doce": this.nDoce ? this.nDoce : 0,
            "trece": this.nTrece ? this.nTrece : 0,
            "catorce": this.nCatorce ? this.nCatorce : 0,
            "quince": this.nQuince ? this.nQuince : 0,
            "diezyseis": this.nDiez_y_seis ? this.nDiez_y_seis : 0,
            "Total": this.nTotal,
            "Cliente": this.oDatosGteCte.cliente,
            "Gerente": this.oDatosGteCte.gerente,
            "cuota": this.loginUsuario.cuota,
            "costo": this.loginUsuario.costo,
            "totalCuota": this.loginUsuario.cuota * this.nTotal,
            "totalCosto": this.loginUsuario.costo * this.nTotal,
            "idCliente": this.nIdCliente,
            "idGerente": this.nIdGerente
        };
        setTimeout(() => {
            this.params.closeCallback(this.oDatosHorasAsignables);
        }, 500);
    }

    validaHorasPorActividad() {
        setTimeout(() => {
            this.indicadorProceso.show("Validando actividad...", {
                android: {
                    view: this.modal.nativeElement.android
                }
            });
        }, 50);
        this.apiSubscribe = this.reportesService.validaHorasPorActividad(this.nIdActividad, this.loginUsuario.id_empleado)
            .subscribe({
                next: (data) => {
                    let respuestaHoras = JSON.parse(data);
                    if (respuestaHoras.Correct) {
                        if (this.nTotal > respuestaHoras.object) {
                            dialogs.alert({
                                title: 'Reporte de tiempo',
                                message: `Ha excedido el número de horas a cargar en '${this.oDatosActividades.actididad}' para esta quincena\nFavor de modificar el reporte.`,
                                okButtonText: 'Aceptar',
                                cancelable: false
                            });
                        } else {
                            this.agregahorasNoAsignables();
                        }
                    }
                },
                error: () => {
                    this.indicadorProceso.hide();
                    dialogs.alert({
                        title: "Reporte de tiempo",
                        message: "Ocurrio un error al validar la actividad.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    }).then(() => {
                        this.cerrarModal();
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
        this.cancelServicio.push(this.apiSubscribe);
    }

    agregahorasNoAsignables() {
        this.oDatosHorasNoAsignables = {
            "IDHoraNoAsignable": this.oDatosHorasNoAsignables.IDHoraNoAsignable,
            "IDActividad": this.nIdActividad,
            "NomActividad": this.oDatosActividades.actividad,
            "uno": this.nUno ? this.nUno : 0,
            "dos": this.nDos ? this.nDos : 0,
            "tres": this.nTres ? this.nTres : 0,
            "cuatro": this.nCuatro ? this.nCuatro : 0,
            "cinco": this.nCinco ? this.nCinco : 0,
            "seis": this.nSeis ? this.nSeis : 0,
            "siete": this.nSiete ? this.nSiete : 0,
            "ocho": this.nOcho ? this.nOcho : 0,
            "nueve": this.nNueve ? this.nNueve : 0,
            "diez": this.nDiez ? this.nDiez : 0,
            "once": this.nOnce ? this.nOnce : 0,
            "doce": this.nDoce ? this.nDoce : 0,
            "trece": this.nTrece ? this.nTrece : 0,
            "catorce": this.nCatorce ? this.nCatorce : 0,
            "quince": this.nQuince ? this.nQuince : 0,
            "diezyseis": this.nDiez_y_seis ? this.nDiez_y_seis : 0,
            "Total": this.nTotal,
            "cuota": this.loginUsuario.cuota,
            "costo": this.loginUsuario.costo,
            "totalCuota": this.loginUsuario.cuota * this.nTotal,
            "totalCosto": this.loginUsuario.costo * this.nTotal,
            "Descripcion": this.sDescripcion
        };
        this.params.closeCallback(this.oDatosHorasNoAsignables);
    }

    validaFinDeSemana(quincena: any, mes: string, anio: number) {
        let oPropiedadesNumeros = {
            "bUno": false,
            "bDos": false,
            "bTres": false,
            "bCuatro": false,
            "bCinco": false,
            "bSeis": false,
            "bSiete": false,
            "bOcho": false,
            "bNueve": false,
            "bDiez": false,
            "bOnce": false,
            "bDoce": false,
            "bTrece": false,
            "bCatorce": false,
            "bQuince": false,
            "bDiez_y_seis": false,

            "sColorUno": "#EAEDED",
            "sColorDos": "#EAEDED",
            "sColorTres": "#EAEDED",
            "sColorCuatro": "#EAEDED",
            "sColorCinco": "#EAEDED",
            "sColorSeis": "#EAEDED",
            "sColorSiete": "#EAEDED",
            "sColorOcho": "#EAEDED",
            "sColorNueve": "#EAEDED",
            "sColorDiez": "#EAEDED",
            "sColorOnce": "#EAEDED",
            "sColorDoce": "#EAEDED",
            "sColorTrece": "#EAEDED",
            "sColorCatorce": "#EAEDED",
            "sColorQuince": "#EAEDED",
            "sColorDiez_y_seis": "#EAEDED"
        };
        for (let i = quincena === 1 ? 1 : 16; i <= (quincena === 1 ? 15 : 31); i++) {
            let fecha = moment(`${anio}-${mes}-${i}`, "YYYY-MM-DD");
            if (fecha.isValid()) {
                    switch (i) {
                        case 1: case 16:
                            this.oPropiedadesNumeros.bUno = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorUno = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 2: case 17:
                            this.oPropiedadesNumeros.bDos = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorDos = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 3: case 18:
                            this.oPropiedadesNumeros.bTres = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorTres = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 4: case 19:
                            this.oPropiedadesNumeros.bCuatro = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorCuatro = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 5: case 20:
                            this.oPropiedadesNumeros.bCinco = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorCinco = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 6: case 21:
                            this.oPropiedadesNumeros.bSeis = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorSeis = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 7: case 22:
                            this.oPropiedadesNumeros.bSiete = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorSiete = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 8: case 23:
                            this.oPropiedadesNumeros.bOcho = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorOcho = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 9: case 24:
                            this.oPropiedadesNumeros.bNueve = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorNueve = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 10: case 25:
                            this.oPropiedadesNumeros.bDiez = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorDiez = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 11: case 26:
                            this.oPropiedadesNumeros.bOnce = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorOnce = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 12: case 27:
                            this.oPropiedadesNumeros.bDoce = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorDoce = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 13: case 28:
                            this.oPropiedadesNumeros.bTrece = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorTrece = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 14: case 29:
                            this.oPropiedadesNumeros.bCatorce = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorCatorce = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 15: case 30:
                            this.oPropiedadesNumeros.bQuince = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorQuince = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                        case 16: case 31:
                            this.oPropiedadesNumeros.bDiez_y_seis = (fecha.day() === 6 || fecha.day() === 0) ? false: true;
                            this.oPropiedadesNumeros.sColorDiez_y_seis = (fecha.day() === 6 || fecha.day() === 0) ? "#EAEDED" : "#FFFFFF";
                            break;
                    }
                }
        }
    }

    cerrarModal() {
        setTimeout(() => {
            this.params.closeCallback();
        }, 500);
    }
}