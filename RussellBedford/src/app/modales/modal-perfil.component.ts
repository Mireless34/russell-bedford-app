import { Component, NgZone, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as imagepicker from "@nativescript/imagepicker";
import { ModalDialogParams } from '@nativescript/angular';
import { File, isAndroid, knownFolders, path, View } from '@nativescript/core';
import * as camera from "@nativescript/camera";
import * as dialogs from '@nativescript/core/ui/dialogs';
import { ImageSource } from "@nativescript/core/image-source";
import { IndicadorProcesos } from '../shared/indicadorProcesos.service';
import { LoginService } from '../shared/login.service';
import { Subscription } from 'rxjs';
import { getString, setString } from '@nativescript/core/application-settings';
import { ImageCropper, OptionsAndroid } from "nativescript-imagecropper";
import * as permissions from "nativescript-permissions";
import * as moment from 'moment';
moment.updateLocale('es', {
    parentLocale: 'es',
    week: {
        dow: 1
    },
    monthsShort: [
        "Ene.", "Feb.", "Mar.", "Abr.", "May.", "Jun.",
        "Jul.", "Ago.", "Sep.", "Oct.", "Nov.", "Dic."
    ]
});
@Component({
    selector: "ns-modalPerfil",
    templateUrl: "./modal-perfil.component.html",
    providers: [IndicadorProcesos, LoginService]
})

export class ModalPerfil implements OnInit {

    loginUsuario = JSON.parse(getString("loginUsuario"));
    srcImagen: string;
    imagenBase64: any;
    private apiSubscribe: Subscription;
    cancelServicio: any = [];
    private imageCropper: ImageCropper;
    public imageUrl: any;
    @ViewChild('modal', { static: false }) modal: ElementRef<View>;

    constructor(private params: ModalDialogParams, private zone: NgZone, private indicadorProceso: IndicadorProcesos, private loginService: LoginService) {

    }

    ngOnInit(): void {
        this.imageCropper = new ImageCropper();
        this.imageUrl = null;
        this.imagenBase64 = this.params.context.imagenBase64;
        this.srcImagen = "<!DOCTYPE html><html><body><center><img src=" + `data:image/png;base64,${this.imagenBase64}` + " width=\"100%\" heigth=\"100%\"/></center></body></html>"
    }

    opcionActualizarFotoPerfil() {
        dialogs.action({
            title: "Foto de perfil",
            cancelButtonText: "Cancelar",
            actions: ["Cámara", "Galería"],
            cancelable: true
        }).then((result) => {
            if (result === "Cámara") {
                this.zone.run(() => {
                    this.resizePhoto();
                });
            } else if (result === "Galería") {
                this.zone.run(() => {
                    this.abrirExploradorArchivos();
                });
            }
        });
    }

    resizePhoto() {
        const options = { width: 600, height: 600, lockSquare: true };
        if (camera.isAvailable()) {
            permissions
                .requestPermission(isAndroid ? [
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ] : [])
                .then(() => {
                    camera
                        .takePicture({
                            width: 800,
                            keepAspectRatio: true,
                            saveToGallery: false,
                            cameraFacing: "rear",
                        })
                        .then((imageAsset) => {
                            imageAsset.options.height = 600;
                            imageAsset.options.width = 600;
                            this.indicadorProceso.show("Actualizando foto de perfil", {
                                android: {
                                    view: this.modal.nativeElement.android
                                }
                            });
                            ImageSource.fromAsset(imageAsset).then((source) => {
                                setTimeout(
                                    async () => {
                                        this.imageCropper
                                            .show(source, options)
                                            .then((args) => {
                                                if (args.image !== null) {
                                                    this.imageUrl = args.image;
                                                    let base64 = args.image.toBase64String("png", 70);
                                                    this.actualizaFotoPerfil(base64);
                                                }
                                            })
                                            .catch((e) => {
                                                this.noCargoDocumento();
                                            }).then(() => {
                                                this.indicadorProceso.hide();
                                            });
                                    },
                                    1000
                                );
                            });
                        })
                        .catch((err) => {
                            this.noCargoDocumento();
                        });
                })
                .catch(() => {
                    // When user denies permission
                    dialogs.alert({
                        title: 'Permiso de acceso a la camara',
                        message: 'Si desea acceder a la camara, debe dar permisos manualmente a la aplicación Reporte de tiempo.',
                        okButtonText: 'Aceptar',
                        cancelable: false
                    });
                });
        }
    }

    abrirExploradorArchivos() {
        let context = imagepicker.create({
            mode: "single",
            mediaType: 1
        });
        const options = { width: 600, height: 600, lockSquare: true };
        context
            .authorize()
            .then(() => {
                context.present()
                    .then((selection) => {
                        let imageAsset = selection.length > 0 ? selection[0] : null;
                        if (imageAsset) {
                            this.indicadorProceso.show("Actualizando foto de perfil", {
                                android: {
                                    view: this.modal.nativeElement.android
                                }
                            });
                            imageAsset.options.height = 900;
                            imageAsset.options.width = 900;
                            imageAsset.options.keepAspectRatio = true;
                            const tempFolderPath = knownFolders.temp().getFolder("nsimagepicker").path;
                            const fileName: string = "foto" + "_" + moment().format("DDMMYYYY_HHmmss") + ".png";
                            const tempFilePath = path.join(tempFolderPath, fileName);
                            ImageSource.fromAsset(imageAsset).then(source => {
                                const guardaFoto = source.saveToFile(tempFilePath, "png");
                                if (guardaFoto) {
                                    const sourceFile: File = File.fromPath(tempFilePath);
                                    ImageSource.fromAsset(imageAsset)
                                        .then(source => {
                                            let base64 = source.toBase64String("png", 30);
                                            setTimeout(
                                                async () => {
                                                    this.imageCropper
                                                        .show(source, options)
                                                        .then((args) => {
                                                            if (args.image !== null) {
                                                                this.imageUrl = args.image;
                                                                let base64 = args.image.toBase64String("png", 70);
                                                                this.actualizaFotoPerfil(base64);
                                                            }
                                                        })
                                                        .catch((e) => {
                                                            this.noCargoDocumento();
                                                        });
                                                },
                                                1000
                                            );

                                        })
                                        .catch(err => {
                                            this.noCargoDocumento();
                                        });
                                }
                            });
                        }
                    }).catch((e) => {
                        this.noCargoDocumento();
                    });
            });
    }

    tomarFoto() {
        camera.requestPermissions().then(() => {
            camera.takePicture({ saveToGallery: false })
                .then((imageAsset) => {
                    if (isAndroid) {
                        imageAsset.options.height = 600;
                        imageAsset.options.width = 600;
                        imageAsset.options.keepAspectRatio = true;
                        this.indicadorProceso.show("Actualizando foto de perfil", {
                            android: {
                                view: this.modal.nativeElement.android
                            }
                        });
                        const sourceFile: File = File.fromPath(imageAsset.android);
                        ImageSource.fromAsset(imageAsset)
                            .then(source => {
                                let base64 = source.toBase64String("png", 70);
                                this.actualizaFotoPerfil(base64);
                            })
                            .catch(err => {
                                console.log(err);
                            });
                    }
                }).catch((err) => {
                });
        }, () => {
            dialogs.alert({
                title: 'Permiso de acceso a la camara',
                message: 'Si desea acceder a la camara, debe dar permisos manualmente a la aplicación Reporte de tiempo.',
                okButtonText: 'Aceptar',
                cancelable: false
            });
        }
        );
    }

    actualizaFotoPerfil(Base64: any) {
        this.apiSubscribe = this.loginService.actualizaFotoPerfil(this.loginUsuario.id_empleado, Base64)
            .subscribe({
                next: (oRespuesta) => {
                    let respuesta = JSON.parse(oRespuesta);
                    if (respuesta.Correct) {
                        setTimeout(() => {
                            this.indicadorProceso.hide();
                            this.imagenBase64 = Base64;
                            this.loginUsuario.ProfilePicture = Base64;
                            setString("loginUsuario", JSON.stringify(this.loginUsuario));
                            this.srcImagen = "<!DOCTYPE html><html><body><center><img src=" + `data:image/png;base64,${Base64}` + " width=\"100%\" heigth=\"100%\"/></center></body></html>"
                        }, 50);
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    this.noCargoDocumento();
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
        this.cancelServicio.push(this.apiSubscribe);
    }

    noCargoDocumento() {
        this.indicadorProceso.hide();
        dialogs.alert({
            title: 'Foto de perfil',
            message: 'No se pudo actualizar foto de perfil, favor de intentarlo nuevamente.',
            okButtonText: 'Aceptar',
            cancelable: false
        });
    }

    eliminaFotoPerfil() {
        dialogs.confirm({
            title: "Foto de perfil",
            message: "¿Desea eliminar la foto de perfil?",
            okButtonText: "Eliminar",
            cancelButtonText: "Cancelar",
            cancelable: false
        }).then(result => {
            if (result) {
                this.indicadorProceso.show("Actualizando foto de perfil", {
                    android: {
                        view: this.modal.nativeElement.android
                    }
                });
                this.apiSubscribe = this.loginService.eliminaFotoPerfil(this.loginUsuario.id_empleado)
                    .subscribe({
                        next: (oRespuesta) => {
                            let respuesta = JSON.parse(oRespuesta);
                            if (respuesta.Correct) {
                                this.imagenBase64 = null;
                                this.loginUsuario.ProfilePicture = null;
                                setString("loginUsuario", JSON.stringify(this.loginUsuario));
                                this.cerrarModal();
                            }
                        },
                        error: (error) => {
                            this.indicadorProceso.hide();
                            dialogs.alert({
                                title: "Reporte de tiempo",
                                message: "No se pudo actualizar la foto de perfil, favor de intentarlo más tarde.",
                                okButtonText: "Aceptar",
                                cancelable: false
                            });
                        },
                        complete: () => {
                            this.indicadorProceso.hide();
                        }
                    });
                this.cancelServicio.push(this.apiSubscribe);
            }
        });
    }

    onWebViewLoaded(webargs) {
        const webview = webargs.object;
        if (isAndroid) {
            // Disabled the native zoom control (to enable gestures on Android)
            webview.android.getSettings().setDisplayZoomControls(false);
        }
    }

    cerrarModal() {
        this.params.closeCallback(this.loginUsuario.ProfilePicture);
    }
}