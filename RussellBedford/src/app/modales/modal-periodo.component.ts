import { Component, OnDestroy, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ModalDialogParams } from "@nativescript/angular";
import * as application from '@nativescript/core/application';
import { AndroidApplication, View } from "@nativescript/core";
import { isAndroid } from "@nativescript/core";
import { AndroidActivityBackPressedEventData } from '@nativescript/core';
import { IndicadorProcesos } from "../shared/IndicadorProcesos.service";
import { ReportesService } from "../shared/reportes.service";
import * as dialogs from '@nativescript/core/ui/dialogs';
import { getString } from "@nativescript/core/application-settings";
import { exit } from "nativescript-exit";
import { Subscription } from "rxjs";
@Component({
    selector: "periodo",
    moduleId: module.id,
    templateUrl: "modal-periodo.component.html",
    styleUrls: ["modal-universal.css"],
    providers: [IndicadorProcesos, ReportesService]
})

export class modalPeriodoComponent implements OnInit, OnDestroy {

    loginUsuario = JSON.parse(getString("loginUsuario"));
    cancelaServicio: any;
    oPeriodo: any;
    aQuincena: any = ["Seleccione una opción", "1", "2"];
    aMes: any = [];
    aListaMeses: any = [];
    aAnio: any = [];
    aListaAnios: any = [];
    bContinuar: boolean = false;
    private apiSubscribe: Subscription;
    cancelServicio: any = [];
    @ViewChild('modal', { static: false }) modal: ElementRef<View>;

    constructor(private params: ModalDialogParams, private indicadorProceso: IndicadorProcesos,
        private reportesService: ReportesService) {
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                this.cerrarModal();
            });
        } else {
            // this.esAndroid = false;
            return;
        }
    }

    ngOnInit() {
        this.cancelaServicio = [];
        this.oPeriodo = {
            quincena: "Seleccione una opción",
            mes: "Seleccione una opción",
            ano: "Seleccione una opción"
        };
        this.aMes = [];
        this.aMes.push("Seleccione una opción");
        this.aAnio = [];
        this.aAnio.push("Seleccione una opción");
        this.recuperaMeses();
        this.recuperaAnios();
    }

    ngOnDestroy() {
        if (this.cancelaServicio.length > 0) {
            this.cancelaServicio.forEach(c => {
                c.unsubscribe();
            });
        }
        this.indicadorProceso.hide();
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                dialogs.confirm({
                    title: "Cerrar Reporte de tiempo",
                    message: "¿Desea cerrar la aplicación?",
                    okButtonText: "Si",
                    cancelButtonText: "No",
                    cancelable: false
                }).then(result => {
                    if (result) {
                        exit();
                    }
                });
            });
        } else {
            return;
        }
    }

    recuperaMeses() {
        setTimeout(() => {
            this.indicadorProceso.show("Cargando datos...", {
                android: {
                    view: this.modal.nativeElement.android
                }
            });
        });
        this.apiSubscribe = this.reportesService.getMeses()
            .subscribe({
                next: (respuestaMeses) => {
                    let listaMeses = JSON.parse(respuestaMeses);
                    this.aListaMeses = [];
                    this.aListaMeses = listaMeses.Objects;
                    if (listaMeses.Correct) {
                        this.aMes = [];
                        this.aMes.push("Seleccione una opción");
                        this.aListaMeses.forEach(elemento => {
                            this.aMes.push(elemento.Mes);
                        });
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.alert({
                        title: "Reporte de tiempo",
                        message: "Error al cargar el periodo de tiempo. Inténtelo mas tarde.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
        this.cancelaServicio.push(this.apiSubscribe);
    }

    recuperaAnios() {
        setTimeout(() => {
            this.indicadorProceso.show("Cargando datos...", {
                android: {
                    view: this.modal.nativeElement.android
                }
            });
        });
        this.apiSubscribe = this.reportesService.getAnios()
            .subscribe({
                next: (respuestaAnios) => {
                    let listaAnios = JSON.parse(respuestaAnios);
                    this.aListaAnios = listaAnios.Objects;
                    if (listaAnios.Correct) {
                        this.aAnio = [];
                        this.aAnio.push("Seleccione una opción");
                        this.aListaAnios.forEach(elemento => {
                            this.aAnio.push(String(elemento.Año));
                        });
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.alert({
                        title: "Reporte de tiempo",
                        message: "Error al cargar el periodo de tiempo. Inténtelo mas tarde.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
        this.cancelaServicio.push(this.apiSubscribe);
    }

    nuevoPeriodo() {
        this.indicadorProceso.show("Validando periodo de tiempo...", {
            android: {
                view: this.modal.nativeElement.android
            }
        });
        this.apiSubscribe = this.reportesService.getValidaPeriodo(this.oPeriodo.quincena, this.oPeriodo.mes, this.oPeriodo.ano, this.loginUsuario.id_empleado)
            .subscribe({
                next: (respuesta) => {
                    let existePeriodo = JSON.parse(respuesta);
                    if (existePeriodo.Correct && !existePeriodo.Object.ExistePeriodo) {
                        this.params.closeCallback(this.oPeriodo);
                    } else {
                        dialogs.alert({
                            title: "Reporte de tiempo",
                            message: `Ya existe un registro en la base de datos con el periodo seleccionado.\nFavor de verificarlo.`,
                            okButtonText: "Aceptar",
                            cancelable: false
                        });
                    }
                },
                error: () => {
                    this.indicadorProceso.hide();
                    dialogs.alert({
                        title: "Reporte de tiempo",
                        message: "Ocurrio un error al validar su reporte de tiempo.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
        this.cancelServicio.push(this.apiSubscribe);
    }

    onPropertyValidatedPeriodo(arg) {
        const dataForm = arg.object;
        const quincena = dataForm.getPropertyByName("quincena");
        const mes = dataForm.getPropertyByName("mes");
        const ano = dataForm.getPropertyByName("ano");
        this.bContinuar = false;
        if (quincena.valueCandidate !== "Seleccione una opción" && mes.valueCandidate !== "Seleccione una opción" && ano.valueCandidate !== "Seleccione una opción") {
            this.bContinuar = true;
        }
    }

    cerrarModal() {
        setTimeout(() => {
            this.params.closeCallback();
        }, 200);
    }
}