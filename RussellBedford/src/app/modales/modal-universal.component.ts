import { Component, NgZone, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';

@Component({
    selector: 'universal',
    moduleId: module.id,
    templateUrl: 'modal-universal.component.html',
    styleUrls:["modal-universal.css"],
    providers:[]
})


export class modalUniversalComponent implements OnInit, OnDestroy {

    constructor() {
        
    }

    ngOnInit(): void {
        
    }

    ngOnDestroy(): void {
        
    }
}