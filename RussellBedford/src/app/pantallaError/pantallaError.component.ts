import { Component, NgZone, OnDestroy, OnInit, ViewContainerRef } from "@angular/core";
import { AndroidApplication, isAndroid, Page, File, path, knownFolders } from '@nativescript/core';
import { exit } from "nativescript-exit";
import * as application from "@nativescript/core/application"; 
import { AndroidActivityBackPressedEventData } from "@nativescript/core";
import * as dialogs from '@nativescript/core/ui/dialogs';
import { Connectivity } from '@nativescript/core'
import { RouterExtensions } from "@nativescript/angular";
import { request, HTTPFormData, HTTPFormDataEntry } from "@klippa/nativescript-http";
import { HttpResponse } from "@nativescript/core/http";
import { getString, setString } from "@nativescript/core/application-settings";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ConfiguraAPI } from "../shared/configuraAPI";
@Component({
  selector: "pantallaError",
  moduleId: module.id,
  templateUrl: "pantallaError.component.html",
  styleUrls: ["pantallaError.component.css"],
})

export class PantallaErrorComponent implements OnInit, OnDestroy {

  imagenFueraLinea: string;
  tituloError: string;
  subTituloError: string;
  loginUsuario = JSON.parse(getString("loginUsuario"));
  bconexion: boolean = false;

  constructor(private page: Page, private zone: NgZone, private routerext: RouterExtensions) {
    if (isAndroid) {
      application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        data.cancel = true;
        dialogs.confirm({
          title: "Cerrar Reporte de tiempo",
          message: "¿Desea cerrar la aplicación?",
          okButtonText: "Si",
          cancelButtonText: "No",
          cancelable: false
        }).then(result => {
          if (result) {
            exit();
          }
        });
      });
    }
  }

  ngOnInit(): void {
    this.comprobarConexionInternet();
    setTimeout(() => {
      const sideDrawer = <RadSideDrawer>application.getRootView();
      sideDrawer.gesturesEnabled = false;
  }, 500);
  }

  ngOnDestroy(): void {

  }

  comprobarConexionInternet() {
    this.tituloError = "¡Parece que no hay internet!";
    this.subTituloError = "Revice su conexión para seguir navegando.";
    this.bconexion = true;
    // Get the current connection type
    //https://docs.nativescript.org/http.html#request
    const type = Connectivity.getConnectionType()
    switch (type) {
      case Connectivity.connectionType.none:
        console.log('No connection')
        this.randomImagenOffLine('mantenimiento');
        break
      // case Connectivity.connectionType.wifi:
      //   console.log('WiFi connection');
      //   this.compruebaConexionApi();
      //   break
      // case Connectivity.connectionType.vpn:
      //   console.log('VPN connection')
      //   break
      // case Connectivity.connectionType.mobile:
      //   console.log('Mobile connection')
      //   break
      // case Connectivity.connectionType.ethernet:
      //   console.log('Ethernet connection')
      //   break
      // case Connectivity.connectionType.bluetooth:
      //   console.log('Bluetooth connection')
      //   break
      default:
        this.compruebaConexionApi();
        break
    }
    // Starts monitoring the network for changes
    Connectivity.startMonitoring(newConnectionType => {
      switch (newConnectionType) {
        case Connectivity.connectionType.none:
          console.log('Connection type changed to none.')
          break
        case Connectivity.connectionType.wifi:
          console.log('Connection type changed to WiFi.')
          break
        case Connectivity.connectionType.vpn:
          console.log('Connection type changed to VPN.')
          break
        case Connectivity.connectionType.mobile:
          console.log('Connection type changed to mobile.')
          break
        case Connectivity.connectionType.ethernet:
          console.log('Connection type changed to ethernet.')
          break
        case Connectivity.connectionType.bluetooth:
          console.log('Connection type changed to bluetooth.')
          break
        default:
          break
      }
    })
    // Stops monitoring the connection
    Connectivity.stopMonitoring()
  }

  compruebaConexionApi() {
    this.tituloError = "Ha habido un problema.";
    this.subTituloError = "Estamos teniendo problemas para cargar la página."
    request({
      url: `${ConfiguraAPI.API_URL}cliente/gerentesConProyecto`,
      method: "GET",
    }).then((response: HttpResponse) => {
      if (response.statusCode === 200) {
        setTimeout(() => {
          this.bconexion = false;
          this.zone.run(() => {
            this.routerext.navigate(['./reportes'], { clearHistory: true, animated: true, transition: { name: "slideLeft" } })
          });
        }, 500);
      } else {
        this.randomImagenOffLine('mantenimiento');
      }
    }, (e) => {
      this.randomImagenOffLine('espacio');
    });
  }

  randomImagenOffLine(tipoFalla: string) {
    setTimeout(() => {
      this.bconexion = false;
      let numero = Math.round(Math.random() * 10);
      let filePath = path.join(knownFolders.documents().path, `/app/app/imagenes/offLine/${tipoFalla}/${numero}.png`);
      let exists = File.exists(filePath);
      this.imagenFueraLinea = exists ? filePath : `~/app/imagenes/offLine/${tipoFalla}/default.png`;
    }, 1000);
  }

  configuraIP() {
    if (this.imagenFueraLinea.includes('espacio')) {
    dialogs.prompt({
      title: 'Contraseña',
      inputType: dialogs.inputType.password,
      message: 'Ingrese la contraseña',
      cancelable: false,
      okButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
    }).then((data) => {
      if (data.result) {
        if ((<any>global).btoa(data.text) === 'U2lzdGVtYXMjMjAyMw==') {
          dialogs.prompt({
            title: 'Configuración de IP',
            inputType: dialogs.inputType.phone,
            message: 'Configura la dirección IP para completar la conexión',
            cancelable: false,
            okButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            defaultText: getString('DireccionIP')
          }).then((data) => {
            if (data.result) {
              if (data.text) {
                setString('DireccionIP', (data.text));
                ConfiguraAPI.API_URL = `http://${data.text}/stuxnet/api/`;
                setTimeout(() => {
                  this.comprobarConexionInternet();
                }, 200);
              }
            }
          });
        } else {
          dialogs.alert({
            title:"Contraseña",
            message: "La contraseña es incorrecta, favor de intentarlo nuevamente",
            okButtonText: 'Aceptar',
            cancelable: false
          })
        }
      }
    });
    }
  }
}