import { Component, NgZone, OnInit, ViewContainerRef } from "@angular/core";
import { DrawerStateChangingEventArgs, RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ModalDialogOptions, ModalDialogService } from '@nativescript/angular';
import Theme from "@nativescript-community/css-theme";
import * as application from '@nativescript/core/application';
import * as app from '@nativescript/core/application';
import * as appversion from "@nativescript/appversion";
import { ApplicationSettings, isIOS, knownFolders, OrientationChangedEventData, path, File } from '@nativescript/core';
import { ToastDuration, ToastPosition, Toasty } from "@triniwiz/nativescript-toasty";
import { RouterExtensions } from "@nativescript/angular";
import * as dialogs from '@nativescript/core/ui/dialogs';
import { firebase } from "@nativescript/firebase-core";
import { NavigationEnd, Router } from "@angular/router";
import { filter } from "rxjs/operators";
import { getBoolean, getString, remove, setBoolean, setString } from "@nativescript/core/application-settings";
import { ModalPerfil } from "./modales/modal-perfil.component";
import { removeBadge, setBadge } from "nativescript-plugin-badge-ns-7";
import { PushNotificationService } from "./shared/push-notification.service";
import { ConfiguraAPI } from "./shared/configuraAPI";
require("~/app/shared/cadenaAbase64");
@Component({
    selector: "ns-app",
    templateUrl: "app.component.html"
})
export class AppComponent implements OnInit {

    loginUsuario = getString("loginUsuario") ? JSON.parse(getString("loginUsuario")) : null;
    sNombreCompleto: string = "";
    sInicialesUsuario: string;
    versionApp = "";
    imagenFueraLinea: string = `~/app/imagenes/coorporativas/1.png`;

    constructor(private routerext: RouterExtensions, private zone: NgZone, private router: Router, private viewContainerRef: ViewContainerRef,
        private modalService: ModalDialogService, private pushService: PushNotificationService) {
            ConfiguraAPI.API_URL = getString('DireccionIP') ? `http://${getString('DireccionIP')}/stuxnet/api/` : ConfiguraAPI.API_URL;
        appversion.getVersionName().then((v: string) => {
            this.versionApp = "versión: " + v;
        });
    }

    ngOnInit() {
        this.router.events
            .pipe(filter((event: any) => event instanceof NavigationEnd))
            .subscribe((event: NavigationEnd) => {
                if (this.loginUsuario) {
                    firebase().analytics().logEvent("Empleado_" + this.loginUsuario.num_empleado, {
                        "paginas_visitadas": event.urlAfterRedirects
                    });
                }
            });
        application.on(application.orientationChangedEvent, (args: OrientationChangedEventData) => {
            this.onOrientationChanged(args);
        });
        // removeBadge();
        if (application.android) {
            try {
                Theme.toggleDarkMode(false);
            } catch (e) {
                console.log("Error setting Theme to light mode", e);
            }
        }
        setTimeout(() => {
            if (!getString("usuario") && !getString("contrasena")) {
                const sideDrawer = <RadSideDrawer>application.getRootView();
                sideDrawer.gesturesEnabled = false;
            }
        }, 1500);
    }

    onOrientationChanged(evt) {
        this.zone.run(() => {
            if (evt.newValue === "landscape") {
                setBoolean("portrait", false);
            } else {
                setBoolean("portrait", true);
            }
        });
    }

    abrirFotoPerfil() {
        const options: ModalDialogOptions = {
            animated: true,
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {
                ruta: "",
                imagenBase64: this.loginUsuario.ProfilePicture,
            },
            cancelable: true,
            stretched: false
        };
        this.modalService.showModal(ModalPerfil, options)
            .then((dialogResult: string) => {
                this.loginUsuario.ProfilePicture = dialogResult;
            });
    }

    abriendoLateral(args: DrawerStateChangingEventArgs) {
        if (ApplicationSettings.getString("loginUsuario")) {
            this.randomImagenOffLine();
            this.loginUsuario = JSON.parse(getString("loginUsuario"));
            this.sNombreCompleto = `${this.loginUsuario.nom_empleado} ${this.loginUsuario.apellido}`;
            this.sInicialesUsuario = (this.loginUsuario.nom_empleado).toUpperCase().substring(0, 1) + (this.loginUsuario.apellido).toUpperCase().substring(0, 1);
        }
    }

    randomImagenOffLine() {
        let numero = Math.round(Math.random() * 10);
        console.log("Imagen a mostrar:", numero);
        let filePath = path.join(knownFolders.documents().path, `/app/app/imagenes/coorporativas/${numero}.png`);
        let exists = File.exists(filePath);
        this.imagenFueraLinea = exists ? filePath : `~/app/imagenes/coorporativas/1.png`;
      }

    seleccionaOpcionMenu(opcion: string) {
        this.cierraLateral();
        setTimeout(() => {
            this.zone.run(() => {
                this.routerext.navigate([`./${opcion}`], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
            });
        }, 200);
    }
    
    cerrarSesion() {
        dialogs.confirm({
            title: "Cerrar Reporte de Tiempo",
            message: "¿Desea cerrar sesión?",
            okButtonText: "Si",
            cancelButtonText: "No"
        }).then(result => {
            if (result) {
                // setBadge(0);
                remove("usuario");
                remove("contrasena");
                remove("loginUsuario");
                remove("portrait");
                remove("aReportes");
                this.cierraLateral();
                this.routerext.navigate([""], { clearHistory: true })
            }
        });
    }

    cierraLateral() {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.closeDrawer();
    }
}
