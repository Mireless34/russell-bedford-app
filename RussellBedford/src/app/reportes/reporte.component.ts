import { Component, NgZone, OnDestroy, OnInit, ViewContainerRef, ElementRef, ViewChild } from "@angular/core";
import { ModalDialogOptions, ModalDialogService, RouterExtensions } from "@nativescript/angular";
import { Subscription } from "rxjs";
import { ActivatedRoute, Router } from "@angular/router";
import { AndroidApplication, isAndroid } from '@nativescript/core';
import * as application from '@nativescript/core/application';
import { OrientationChangedEventData } from '@nativescript/core';
import * as dialogs from '@nativescript/core/ui/dialogs';
import { IndicadorProcesos } from "../shared/IndicadorProcesos.service";
import { ReportesService } from "~/app/shared/reportes.service";
import { SegmentedBar, SegmentedBarItem } from "@nativescript/core";
import { SelectedIndexChangedEventData } from '@nativescript-community/ui-material-bottom-navigation';
import { modalHorasComponent } from "~/app/modales/modal-horas.component";
import { AndroidActivityBackPressedEventData } from '@nativescript/core';
import { ApplicationSettings } from '@nativescript/core';
import { getString, remove, setString } from "@nativescript/core/application-settings";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { exit } from "nativescript-exit";

const moment = require("moment");
moment.updateLocale('es', { parentLocale: 'es' });
@Component({
    selector: "Reporte",
    templateUrl: "reporte.component.html",
    styleUrls: ["reportes.component.css"],
    providers: [ReportesService, IndicadorProcesos]
})

export class ReporteComponent implements OnInit, OnDestroy {

    loginUsuario = JSON.parse(getString("loginUsuario"));
    oPeriodo = JSON.parse(getString("oPeriodoTiempo"));
    aReportes = JSON.parse(getString("aReportes"));
    nQuincena: number;
    tipodeHoras: Array<SegmentedBarItem> = [];
    nAsignableNoAsignable: number;
    aReporte: any;
    oTotalHrsAsignables: any;
    nTotalHorasAsignables: number = 0;
    aHorasAsignables: any;
    oTotalHrsNoAsignables: any;
    nTotalHorasNoAsignables: number = 0;
    aHorasNoAsignables: any;
    bAgregaHoras: boolean = ApplicationSettings.getBoolean("portrait");
    tipohoras: string = "asignables";
    private apiSubscribe: Subscription;
    cancelServicio: any = [];
    bModificado: boolean = false;
    aActividadesEspeciales: any;

    @ViewChild("tabview", { static: false }) tabview: ElementRef;

    constructor(private zone: NgZone, private routerext: RouterExtensions, private activatedRouter: ActivatedRoute, private reportesService: ReportesService,
        private viewContainerRef: ViewContainerRef, private modalService: ModalDialogService, private indicadorProceso: IndicadorProcesos, private router: Router) {
        let f = new SegmentedBarItem();
        f.title = "Horas Asignables";
        this.tipodeHoras.push(f);
        let m = new SegmentedBarItem();
        m.title = "Horas No Asignables";
        this.tipodeHoras.push(m);
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                this.regresaAReportes();
            });
        } else {
            // this.esAndroid = false;
            return;
        }
    }

    ngOnInit() {
        application.on(application.orientationChangedEvent, (args: OrientationChangedEventData) => {
            this.onOrientationChanged(args);
        });
        this.aActividadesEspeciales = {
            horaslimite: 8,
            nivel: 300,
            actividades: [{
                id: 6,
                nombre: 'Enfermedad',
                soporte: 'Incapacidad'
            }, {
                id: 7,
                nombre: 'Permiso con goce de sueldo',
                soporte: 'Correo electrónico'
            }, {
                id: 9,
                nombre: 'Permiso sin goce de sueldo',
                soporte: 'Correo electrónico'
            }, {
                id: 10,
                nombre: 'Vacaciones',
                soporte: 'Formato de vacaciones'
            }]
        };
        this.aHorasAsignables = [];
        this.aHorasNoAsignables = [];
        this.bModificado = true;
        this.nQuincena = Number(this.oPeriodo.quincena);
        this.agregaEncabezadoHrsAsignables(this.nQuincena);
        this.agregaEncabezadoHrsNoAsignables(this.nQuincena);
        this.oPeriodo.id_tiempo && this.recuperaReporte(this.oPeriodo.id_tiempo);
        const sideDrawer = <RadSideDrawer>application.getRootView();
        sideDrawer.gesturesEnabled = false;
    }

    ngOnDestroy() {
        remove("oPeriodoTiempo");
        if (this.cancelServicio.length > 0) {
            this.cancelServicio.forEach(c => {
                if (c) {
                    c.unsubscribe();
                }
            });
        }
        if (isAndroid) {
            application.android.off(AndroidApplication.activityBackPressedEvent);
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                dialogs.confirm({
                    title: "Cerrar Reporte de tiempo",
                    message: "¿Desea cerrar la aplicación?",
                    okButtonText: "Si",
                    cancelButtonText: "No",
                    cancelable: false
                }).then(result => {
                    if (result) {
                        exit();
                    }
                });
            });
        } else {
            return;
        }
    }

    onOrientationChanged(evt) {
        this.zone.run(() => {
            if (evt.newValue === "landscape") {
                this.bAgregaHoras = false;
                ApplicationSettings.setBoolean("portrait", false);
            } else {
                this.bAgregaHoras = true;
                ApplicationSettings.setBoolean("portrait", true);
            }
        });
    }

    recuperaReporte(nIdTiempo: number) {
        this.bModificado = false;
        this.indicadorProceso.show("Cargando reporte de tiempo...");
        this.apiSubscribe = this.reportesService.getReporte(nIdTiempo)
            .subscribe({
                next: (respuestaReporte) => {
                    let respuesta = JSON.parse(respuestaReporte);
                    this.aReporte = [];
                    this.aReporte = respuesta.Objects;
                    if (this.aReporte) {
                        if (this.aReporte[0].HorasAsignables) {
                            this.aHorasAsignables = [];
                            this.aHorasAsignables = (this.aReporte[0].HorasAsignables);
                            this.realizaSumaTotalHrsAsignables();
                        }
                        if (this.aReporte[0].HorasNoAsignables) {
                            this.aHorasNoAsignables = [];
                            this.aHorasNoAsignables = (this.aReporte[0].HorasNoAsignables);
                            this.realizaSumaTotalHrsNoAsignables();
                        }
                    } else {
                        dialogs.alert({
                            title: "Reporte de tiempo",
                            message: "No se pudieron recuperar los datos del reporte de tiempo.",
                            okButtonText: "Aceptar",
                            cancelable: false
                        }).then(() => {
                            this.irAReportes();
                        });
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.alert({
                        title: "Reporte de tiempo",
                        message: "No se pudieron recuperar los datos del reporte de tiempo.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    }).then(() => {
                        this.irAReportes();
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
        this.cancelServicio.push(this.apiSubscribe);
    }

    seleccionaTipoHoras(args: SelectedIndexChangedEventData) {
        const picker = args.object as SegmentedBar;
        switch (picker.selectedIndex) {
            case 0:
                this.nAsignableNoAsignable = picker.selectedIndex;
                this.tipohoras = "asignables";
                break;
            case 1:
                this.nAsignableNoAsignable = picker.selectedIndex;
                this.tipohoras = "noAsignables";
                break;
        };
    }

    agregaHoras() {
        const options: ModalDialogOptions = {
            animated: true,
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {
                reporte: this.oPeriodo,
                oAsignables: this.tipohoras === "asignables" ? { Clave: "" } : { NomActividad: "" },
                asignablesNoAsignables: this.tipohoras === "asignables" ? this.aHorasAsignables : this.aHorasNoAsignables,
                tipoHoras: this.tipohoras
            },
            cancelable: false,
            stretched: true
        }
        this.modalService.showModal(modalHorasComponent, options)
            .then((dialogResult: string) => {
                if (dialogResult) {
                    setTimeout(() => {
                        if (this.tipohoras === "asignables") {
                            this.aHorasAsignables.pop();
                            this.aHorasAsignables.shift();
                            this.aHorasAsignables.push(dialogResult);
                            this.realizaSumaTotalHrsAsignables();
                            this.bModificado = true;
                        } else {
                            this.aHorasNoAsignables.pop();
                            this.aHorasNoAsignables.shift();
                            this.aHorasNoAsignables.push(dialogResult);
                            this.realizaSumaTotalHrsNoAsignables();
                            this.bModificado = true;
                        }
                    }, 500);
                }
            });
    }

    /************
     * HORAS ASIGNABLES
     ***********/
    //#region
    seleccionaHoraAsignable(oAsignable: any) {
        let index = this.aHorasAsignables.findIndex(item => item === oAsignable);
        if (oAsignable.IDProyecto) {
            dialogs.action({
                title: `Proyecto: ${oAsignable.Clave}`,
                cancelButtonText: "Cancelar",
                actions: ["Editar", "Eliminar"],
                cancelable: false
            }).then((result) => {
                if (result === 'Editar') {
                    const options: ModalDialogOptions = {
                        animated: true,
                        viewContainerRef: this.viewContainerRef,
                        fullscreen: true,
                        context: {
                            reporte: this.oPeriodo,
                            oAsignables: oAsignable,
                            asignablesNoAsignables: this.aHorasAsignables,
                            tipoHoras: "asignables"
                        },
                        cancelable: false,
                        stretched: true
                    }
                    this.modalService.showModal(modalHorasComponent, options)
                        .then((dialogResult: string) => {
                            if (dialogResult) {
                                this.aHorasAsignables[index] = dialogResult;
                                this.aHorasAsignables.pop();
                                this.aHorasAsignables.shift();
                                this.realizaSumaTotalHrsAsignables();
                                this.bModificado = true;
                            }
                        });
                } else if (result === 'Eliminar') {
                    dialogs.confirm({
                        title: 'Reporte de tiempo',
                        message: `¿Desea eliminar este elemento?\n\nProyecto: ${oAsignable.Clave}`,
                        okButtonText: "Si",
                        cancelButtonText: "No",
                        cancelable: false
                    }).then(result => {
                        if (result) {
                            setTimeout(() => {
                                this.aHorasAsignables.splice(index, 1);
                                this.aHorasAsignables.pop();
                                this.aHorasAsignables.shift();
                                this.realizaSumaTotalHrsAsignables();
                                this.bModificado = true;
                            }, 500);
                        }
                    });
                }
            });
        }
    }

    realizaSumaTotalHrsAsignables() {
        let uno: number = 0;
        let dos: number = 0;
        let tres: number = 0;
        let cuatro: number = 0;
        let cinco: number = 0;
        let seis: number = 0;
        let siete: number = 0;
        let ocho: number = 0;
        let nueve: number = 0;
        let diez: number = 0;
        let once: number = 0;
        let doce: number = 0;
        let trece: number = 0;
        let catorce: number = 0;
        let quince: number = 0;
        let diezyseis: number = 0;
        let total: number = 0;
        this.aHorasAsignables.forEach(elemento => {
            uno = uno + Number(elemento.uno);
            dos = dos + Number(elemento.dos);
            tres = tres + Number(elemento.tres);
            cuatro = cuatro + Number(elemento.cuatro);
            cinco = cinco + Number(elemento.cinco);
            seis = seis + Number(elemento.seis);
            siete = siete + Number(elemento.siete);
            ocho = ocho + Number(elemento.ocho);
            nueve = nueve + Number(elemento.nueve);
            diez = diez + Number(elemento.diez);
            once = once + Number(elemento.once);
            doce = doce + Number(elemento.doce);
            trece = trece + Number(elemento.trece);
            catorce = catorce + Number(elemento.catorce);
            quince = quince + Number(elemento.quince);
            diezyseis = diezyseis + Number(elemento.diezyseis);
            total = total + Number(elemento.Total);
        });
        this.nTotalHorasAsignables = total;
        this.oTotalHrsAsignables = {
            "ID_Hora_Asignable": null,
            "IDProyecto": 0,
            "Clave": "Total",
            "uno": uno,
            "dos": dos,
            "tres": tres,
            "cuatro": cuatro,
            "cinco": cinco,
            "seis": seis,
            "siete": siete,
            "ocho": ocho,
            "nueve": nueve,
            "diez": diez,
            "once": once,
            "doce": doce,
            "trece": trece,
            "catorce": catorce,
            "quince": quince,
            "diezyseis": diezyseis,
            "Total": total,
            "Cliente": "-",
            "Gerente": "-",
            "cuota": 0,
            "costo": 0,
            "totalCuota": 0,
            "totalCosto": 0,
            "idCliente": 0,
            "idGerente": 0
        };
        this.aHorasAsignables.push(this.oTotalHrsAsignables);
        this.agregaEncabezadoHrsAsignables(this.nQuincena);
    }

    agregaEncabezadoHrsAsignables(quincena: number) {
        this.oTotalHrsAsignables = {
            "ID_Hora_Asignable": null,
            "IDProyecto": 0,
            "Clave": "Proyecto",
            "uno": quincena === 1 ? 1 : 16,
            "dos": quincena === 1 ? 2 : 17,
            "tres": quincena === 1 ? 3 : 18,
            "cuatro": quincena === 1 ? 4 : 19,
            "cinco": quincena === 1 ? 5 : 20,
            "seis": quincena === 1 ? 6 : 21,
            "siete": quincena === 1 ? 7 : 22,
            "ocho": quincena === 1 ? 8 : 23,
            "nueve": quincena === 1 ? 9 : 24,
            "diez": quincena === 1 ? 10 : 25,
            "once": quincena === 1 ? 11 : 26,
            "doce": quincena === 1 ? 12 : 27,
            "trece": quincena === 1 ? 13 : 28,
            "catorce": quincena === 1 ? 14 : 29,
            "quince": quincena === 1 ? 15 : 30,
            "diezyseis": quincena === 1 ? 16 : 31,
            "Total": "Total",
            "Cliente": "Cliente",
            "Gerente": "Gerente",
            "cuota": 0,
            "costo": 0,
            "totalCuota": 0,
            "totalCosto": 0,
            "idCliente": 0,
            "idGerente": 0
        }
        this.aHorasAsignables.unshift(this.oTotalHrsAsignables);
    }
    //#endregion

    /*************
     * HORAS NO ASIGNABLES
     *************/
    //#region
    seleccionaHoraNoAsignable(oNoAsignable: any) {
        let index = this.aHorasNoAsignables.findIndex(item => item === oNoAsignable)
        if (oNoAsignable.IDActividad) {
            dialogs.action({
                title: `Actividad: ${oNoAsignable.NomActividad}`,
                actions: ["Editar", "Eliminar"],
                cancelButtonText: "Cancelar",
                cancelable: false
            }).then((result) => {
                if (result === 'Editar') {
                    const options: ModalDialogOptions = {
                        animated: true,
                        viewContainerRef: this.viewContainerRef,
                        fullscreen: true,
                        context: {
                            reporte: this.oPeriodo,
                            oAsignables: oNoAsignable,
                            asignablesNoAsignables: this.aHorasNoAsignables,
                            tipoHoras: "noAsignables"
                        },
                        cancelable: false,
                        stretched: true
                    }
                    this.modalService.showModal(modalHorasComponent, options)
                        .then((dialogResult: string) => {
                            if (dialogResult) {
                                this.aHorasNoAsignables[index] = dialogResult;
                                this.aHorasNoAsignables.pop();
                                this.aHorasNoAsignables.shift();
                                this.realizaSumaTotalHrsNoAsignables();
                                this.bModificado = true;
                            }
                        });
                } else if (result === 'Eliminar') {
                    this.bModificado = true;
                    // if (oNoAsignable.IDHoraNoAsignable) {
                    dialogs.confirm({
                        title: 'Reporte de tiempo',
                        message: `¿Desea eliminar este elemento?\n\nActividad: ${oNoAsignable.NomActividad}`,
                        okButtonText: "Si",
                        cancelButtonText: "No",
                        cancelable: false
                    }).then(result => {
                        if (result) {
                            // this.apiSubscribe = this.reportesService.deleteHoraNoAsignable(oNoAsignables.IDHoraNoAsignable, this.nTotalHorasNoAsignables, this.oPeriodo.id_tiempo)
                            //     .subscribe({
                            //         next: (respuestaReportes) => {
                            //             let respuesta = JSON.parse(respuestaReportes)
                            //         },
                            //         error: (error) => {

                            //         },
                            //         complete: () => {

                            //         }
                            //     });
                            setTimeout(() => {
                                this.aHorasNoAsignables.splice(index, 1);
                                this.aHorasNoAsignables.pop();
                                this.aHorasNoAsignables.shift();
                                this.realizaSumaTotalHrsNoAsignables();
                                this.bModificado = true;
                            }, 500);
                        }
                    });
                    // }
                }
            });
        }
    }

    realizaSumaTotalHrsNoAsignables() {
        let uno: number = 0;
        let dos: number = 0;
        let tres: number = 0;
        let cuatro: number = 0;
        let cinco: number = 0;
        let seis: number = 0;
        let siete: number = 0;
        let ocho: number = 0;
        let nueve: number = 0;
        let diez: number = 0;
        let once: number = 0;
        let doce: number = 0;
        let trece: number = 0;
        let catorce: number = 0;
        let quince: number = 0;
        let diezyseis: number = 0;
        let total: number = 0;
        this.aHorasNoAsignables.forEach(elemento => {
            uno = uno + Number(elemento.uno);
            dos = dos + Number(elemento.dos);
            tres = tres + Number(elemento.tres);
            cuatro = cuatro + Number(elemento.cuatro);
            cinco = cinco + Number(elemento.cinco);
            seis = seis + Number(elemento.seis);
            siete = siete + Number(elemento.siete);
            ocho = ocho + Number(elemento.ocho);
            nueve = nueve + Number(elemento.nueve);
            diez = diez + Number(elemento.diez);
            once = once + Number(elemento.once);
            doce = doce + Number(elemento.doce);
            trece = trece + Number(elemento.trece);
            catorce = catorce + Number(elemento.catorce);
            quince = quince + Number(elemento.quince);
            diezyseis = diezyseis + Number(elemento.diezyseis);
            total = total + Number(elemento.Total);
        });
        this.nTotalHorasNoAsignables = total;
        this.oTotalHrsNoAsignables = {
            "ID_Hora_No_Asignable": null,
            "IDActividad": 0,
            "NomActividad": "Total",
            "uno": uno,
            "dos": dos,
            "tres": tres,
            "cuatro": cuatro,
            "cinco": cinco,
            "seis": seis,
            "siete": siete,
            "ocho": ocho,
            "nueve": nueve,
            "diez": diez,
            "once": once,
            "doce": doce,
            "trece": trece,
            "catorce": catorce,
            "quince": quince,
            "diezyseis": diezyseis,
            "Total": total,
            "cuota": 0,
            "costo": 0,
            "totalCuota": 0,
            "totalCosto": 0,
            "descripcion": 0
        }
        this.aHorasNoAsignables.push(this.oTotalHrsNoAsignables);
        this.agregaEncabezadoHrsNoAsignables(this.nQuincena);
    }

    agregaEncabezadoHrsNoAsignables(quincena: number) {
        this.oTotalHrsNoAsignables = {
            "ID_Hora_No_Asignable": null,
            "IDActividad": 0,
            "NomActividad": "Actividad",
            "uno": quincena === 1 ? 1 : 16,
            "dos": quincena === 1 ? 2 : 17,
            "tres": quincena === 1 ? 3 : 18,
            "cuatro": quincena === 1 ? 4 : 19,
            "cinco": quincena === 1 ? 5 : 20,
            "seis": quincena === 1 ? 6 : 21,
            "siete": quincena === 1 ? 7 : 22,
            "ocho": quincena === 1 ? 8 : 23,
            "nueve": quincena === 1 ? 9 : 24,
            "diez": quincena === 1 ? 10 : 25,
            "once": quincena === 1 ? 11 : 26,
            "doce": quincena === 1 ? 12 : 27,
            "trece": quincena === 1 ? 13 : 28,
            "catorce": quincena === 1 ? 14 : 29,
            "quince": quincena === 1 ? 15 : 30,
            "diezyseis": quincena === 1 ? 16 : 31,
            "Total": "Total",
            "cuota": 0,
            "costo": 0,
            "totalCuota": 0,
            "totalCosto": 0,
            "descripcion": "-",
        }
        this.aHorasNoAsignables.unshift(this.oTotalHrsNoAsignables);
    }

    validaHorasNoAsignables() {
        let actividades: string = '', soporte: string = '', cuerpo: string = '';
        this.aHorasNoAsignables.forEach(e => {
            this.aActividadesEspeciales.actividades.forEach(f => {
                if (e.IDActividad === f.id) {
                    actividades += ` - ${e.NomActividad} `;
                    soporte += `<ul><li><b>${e.NomActividad}:</b> ${f.soporte}</ul></li>`;
                }
            });
        });
        cuerpo = `<p>Estimado Gerente, acaba de recibir un correo derivado que una persona a su cargo
        está notificando horas en su reporte de tiempo relacionadas con las siguientes actividades; <b>${actividades}</b>, 
        en la quincena <b>${this.oPeriodo.quincena}</b> del mes de <b>${this.oPeriodo.mes}</b></p>
        Por favor verifica que exista el soporte o formato  adecuado, para cada caso.<br/>
        ${soporte}`;
        // this.enviarEmail('osar8405@gmail.com', 'Reporte de tiempo', cuerpo, '', '');
    }
    //#endregion

    validaTotalHorasPorDia(estadoEnvio: string) {
        let reporteValido: boolean = true;
        for (let i = 3; i <= 18; i++) {
            let totalPorDia: number = 0;
            let nombreDia: string = '';
            this.aHorasAsignables.forEach((e, index) => {
                let oAsignableTemporal = Object.values(e);
                if (index === 0) {
                    nombreDia = String(oAsignableTemporal[i]);
                } else if (index >= 1 && index < this.aHorasAsignables.length - 1) {
                    totalPorDia += Number(oAsignableTemporal[i]);
                }
            });
            this.aHorasNoAsignables.forEach((e, index) => {
                let oNoAsignableTemporal = Object.values(e);
                if (index === 0) {
                    nombreDia = String(oNoAsignableTemporal[i]);
                } else if (index < this.aHorasNoAsignables.length - 1) {
                    totalPorDia += Number(oNoAsignableTemporal[i]);
                }
            });
            if (totalPorDia > this.aActividadesEspeciales.horaslimite) {
                reporteValido = false;
                dialogs.alert({
                    title: 'Reporte de tiempo',
                    message: `La suma de horas asignables y no asignables debe ser ${this.aActividadesEspeciales.horaslimite} para el día ${nombreDia}\nFavor de modificar el reporte`,
                    okButtonText: 'Aceptar',
                    cancelable: false
                });
                return;
            }
        }
        setTimeout(() => {
            if (reporteValido) {
                this.validaPeriodoDeTiempo(estadoEnvio);
            }
        }, 200);
    }

    validaPeriodoDeTiempo(estadoEnvio: string) {
        const enviadoGuardado: string = estadoEnvio === 'Enviado' ? 'Enviar' : 'Guardar';
        dialogs.confirm({
            title: 'Reporte de tiempo',
            message: `¿Desea ${enviadoGuardado} su reporte de tiempo?`,
            okButtonText: `${enviadoGuardado}`,
            cancelButtonText: 'cancelar',
            cancelable: false
        }).then(result => {
            if (result) {
                if (!this.oPeriodo.id_tiempo) {
                    this.indicadorProceso.show("Validando periodo de tiempo...");
                    this.reportesService.getValidaPeriodo(this.oPeriodo.quincena, this.oPeriodo.mes, this.oPeriodo.ano, this.loginUsuario.id_empleado)
                        .subscribe({
                            next: (respuesta) => {
                                let existePeriodo = JSON.parse(respuesta);
                                if (existePeriodo.Correct && existePeriodo.Object.ExistePeriodo) {
                                    dialogs.alert({
                                        title: "Reporte de tiempo",
                                        message: `Ya existe un registro en la base de datos con el periodo seleccionado.\nFavor de verificarlo.`,
                                        okButtonText: "Aceptar",
                                        cancelable: false
                                    });
                                } else {
                                    this.enviaReporte(estadoEnvio);
                                }
                            },
                            error: () => {
                                this.indicadorProceso.hide();
                                dialogs.alert({
                                    title: "Reporte de tiempo",
                                    message: "Ocurrio un error al validar su reporte de tiempo, favor de intentarlo nuevamente.",
                                    okButtonText: "Aceptar",
                                    cancelable: false
                                });
                            },
                            complete: () => {
                                this.indicadorProceso.hide();
                            }
                        });
                } else {
                    this.enviaReporte(estadoEnvio);
                }
            }
        });
    }

    enviaReporte(estadoEnvio: string) {
        const enviadoGuardado: string = estadoEnvio === 'Enviado' ? 'Enviando' : 'Guardando';
        this.indicadorProceso.show(`${enviadoGuardado} reporte de tiempo...`);
        let idFolio: number = 0;
        let fechaReporte = this.oPeriodo.ano + "" + ((moment().month(this.oPeriodo.mes).format("M")).length === 1 ? ("0" + moment().month(this.oPeriodo.mes).format("M")) : moment().month(this.oPeriodo.mes).format("M")) + "" + (this.nQuincena === 1 ? "01" : "16");
        let reporteDeTiempo = {
            idReporte: this.oPeriodo.id_tiempo ? this.oPeriodo.id_tiempo : 0,
            quincena: this.nQuincena,
            mes: this.oPeriodo.mes,
            anio: Number(this.oPeriodo.ano),
            totalHorasAsignables: this.nTotalHorasAsignables,
            totalHorasNoAsignables: this.nTotalHorasNoAsignables,
            total: (this.nTotalHorasAsignables + this.nTotalHorasNoAsignables),
            idEmpleado: this.loginUsuario.id_empleado,
            estatus: estadoEnvio === 'Enviado' ? 1 : 0,
            envio: estadoEnvio === 'Enviado' ? 0 : 1,
            fechaReporte: fechaReporte,
            detalleHorasAsignables: this.aHorasAsignables,
            detalleHorasNoAsignables: this.aHorasNoAsignables
        };
        this.reportesService.saveReporteDeTiempo(this.nQuincena, this.oPeriodo.mes, Number(this.oPeriodo.ano), this.nTotalHorasAsignables, this.nTotalHorasNoAsignables, (this.nTotalHorasAsignables + this.nTotalHorasNoAsignables), this.loginUsuario.id_empleado, 1, 0, fechaReporte, this.aHorasAsignables, this.aHorasNoAsignables)
            .subscribe({
                next: (respuesta) => {
                    let reporte = JSON.parse(respuesta);
                    idFolio = reporte.Object
                    let PeriodoRegistrado = {
                        id_tiempo: this.oPeriodo.id_tiempo ? this.oPeriodo.id_tiempo : idFolio,
                        quincena: this.nQuincena,
                        mes: this.oPeriodo.mes,
                        ano: Number(this.oPeriodo.ano),
                        horas_asignables: this.nTotalHorasAsignables,
                        horas_no_asignables: this.nTotalHorasNoAsignables,
                        total: (this.nTotalHorasAsignables + this.nTotalHorasNoAsignables),
                        envio: estadoEnvio === 'Enviado' ? 1 : 0,
                        estatus: estadoEnvio,
                        color: estadoEnvio === 'Enviado' ? '#28B463' : '#E74C3C',
                        mesAbreviado: this.oPeriodo.mesAbreviado
                    };
                    if (this.oPeriodo.id_tiempo) {
                        let index = this.aReportes.findIndex(item => item.id_tiempo === this.oPeriodo.id_tiempo);
                        this.aReportes[index] = PeriodoRegistrado;
                    } else {
                        this.aReportes.unshift(PeriodoRegistrado);
                    }
                },
                error: () => {
                    this.indicadorProceso.hide();
                    dialogs.alert({
                        title: "Reporte de tiempo",
                        message: "Ocurrio un error al guardar el reporte de tiempo, inténtelo mas tarde.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    });
                },
                complete: () => {
                    this.indicadorProceso.hide();
                    let cuerpo: string = '<p><h3><center>Su reporte de tiempo fue registrado con éxito.</center></h3></p>';
                    cuerpo += `Quincena: <strong>${this.oPeriodo.quincena}</strong><br/>
                    Mes: <strong>${this.oPeriodo.mes}</strong><br/>
                    Año: <strong>${this.oPeriodo.ano}</strong><br/>
                    No. Folio: <strong>${idFolio}</strong>`;
                    // this.enviarEmail('osar8405@gmail.com', 'Reporte de tiempo', cuerpo, '', '');
                    this.validaHorasNoAsignables();
                    dialogs.confirm({
                        title: "Reporte de tiempo",
                        message: "Reporte de tiempo guardado con éxito.",
                        okButtonText: "Aceptar",
                        cancelable: false
                    }).then(resultado => {
                        if (resultado) {
                            setString("aReportes", JSON.stringify(this.aReportes));
                            this.irAReportes();
                        }
                    });
                }
            });
        this.cancelServicio.push(this.apiSubscribe);
    }

    enviarEmail(para: string, titulo: string, cuerpo: string, copia: string, copiaOculta: string) {
        this.apiSubscribe = this.reportesService.enviaCorreo(para, titulo, cuerpo, copia, copiaOculta)
            .subscribe({
                next: (data) => {
                    let respuesta = JSON.parse(data);
                },
                error: (error) => {
                },
                complete: () => {
                }
            });
        this.cancelServicio.push(this.apiSubscribe);
    }

    regresaAReportes() {
        if (this.bModificado) {
            dialogs.confirm({
                title: "Reporte de tiempo",
                message: "¿Desea salir sin enviar o guardar su reporte de tiempo?",
                okButtonText: "Si",
                cancelButtonText: "No",
                cancelable: false
            }).then(resultado => {
                if (resultado) {
                    this.irAReportes();
                }
            });
        } else {
            this.irAReportes();
        }
    }

    irAReportes() {
        setTimeout(() => {
            this.zone.run(() => {
                this.routerext.navigate(["/reportes"], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
            });
        }, 200);
    }
}