import { Component, NgZone, OnDestroy, OnInit, ViewContainerRef } from "@angular/core";
import { Router } from "@angular/router";
import { ModalDialogOptions, ModalDialogService, RouterExtensions } from "@nativescript/angular";
import { isAndroid, OrientationChangedEventData } from '@nativescript/core';
import * as application from '@nativescript/core/application';
import { AndroidActivityBackPressedEventData, AndroidApplication } from "@nativescript/core";
import { IndicadorProcesos } from "~/app/shared/IndicadorProcesos.service";
import * as dialogs from '@nativescript/core/ui/dialogs';
import { getBoolean, getString, remove, setBoolean, setString } from "@nativescript/core/application-settings";
import { exit } from "nativescript-exit";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { ListViewEventData } from "nativescript-ui-listview";
import { ReportesService } from "~/app/shared/reportes.service";
import { modalPeriodoComponent } from "~/app/modales/modal-periodo.component";
import { Subscription } from "rxjs";
import { LoginService } from "../shared/login.service";

@Component({
    selector: "reportes",
    moduleId: module.id,
    templateUrl: "reportes.component.html",
    styleUrls: ["reportes.component.css"],
    providers: [ReportesService, IndicadorProcesos, LoginService]
})

export class ReportesComponent implements OnInit, OnDestroy {

    loginUsuario: any;
    aReportes: any = false;
    oReportes: any;
    private apiSubscribe: Subscription;
    bNuevoPeriodo: boolean = getBoolean("portrait");
    esAndroid: boolean = true;
    aReportesCargando: any;
    cancelServicio: any = [];

    constructor(private modalService: ModalDialogService, private viewContainerRef: ViewContainerRef, private router: Router,
        private reportesService: ReportesService, private zone: NgZone, private routerext: RouterExtensions, private indicadorProceso: IndicadorProcesos) {
        if (!isAndroid) {
            return;
        } else {
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                if (this.router.url === ("/reportes")) {
                    data.cancel = true;
                    dialogs.confirm({
                        title: "Cerrar Reporte de tiempo",
                        message: "¿Desea cerrar la aplicación?",
                        okButtonText: "Si",
                        cancelButtonText: "No",
                        cancelable: false
                    }).then(result => {
                        if (result) {
                            exit();
                        }
                    });
                }
            });
        }
    }

    ngOnInit() {
        application.on(application.orientationChangedEvent, (args: OrientationChangedEventData) => {
            this.onOrientationChanged(args);
        });
        if (application.orientation() === "landscape") {
            this.bNuevoPeriodo = false;
            setBoolean("portrait", false);
        } else {
            this.bNuevoPeriodo = true;
            setBoolean("portrait", true);
        }
        this.reportesPordefault();
        if (getBoolean("notificacionPush")) {
            remove("notificacionPush");
            if (getString("notificacion")) {
                let notificacion = JSON.parse(getString("notificacion"));
                remove("notificacion");
                setTimeout(() => {
                    this.zone.run(() => {
                        this.routerext.navigate([`./${'reporte'}`], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
                    });
                }, 500);
            }
        } else {
            setTimeout(() => {
                if (!getString("aReportes")) {
                    this.recuperaReportes();
                } else {
                    this.aReportes = JSON.parse(getString("aReportes"))
                }
                const sideDrawer = <RadSideDrawer>application.getRootView();
                sideDrawer.gesturesEnabled = true;
            });
        }
    }

    ngOnDestroy() {
        if (this.cancelServicio.length > 0) {
            this.cancelServicio.forEach(c => {
                c.unsubscribe();
            });
        }
    }

    onOrientationChanged(evt) {
        this.zone.run(() => {
            if (evt.newValue === "landscape") {
                this.bNuevoPeriodo = false;
                setBoolean("portrait", false);
            } else {
                this.bNuevoPeriodo = true;
                setBoolean("portrait", true);
            }
        });
    }

    recuperaReportes() {
        this.apiSubscribe = this.reportesService.getReportes(getString("usuario"))
            .subscribe({
                next: (respuestaReportes) => {
                    let respuesta = JSON.parse(respuestaReportes)
                    this.aReportes = [];
                    if (respuesta.Correct) {
                        this.aReportes = respuesta.Objects;
                        this.aReportes.forEach(elemento => {
                            if (elemento.envio === 1) {
                                elemento.estatus = "Enviado";
                                elemento.color = "#28B463";
                            } else if (elemento.envio === 0) {
                                elemento.estatus = "Guardado";
                                elemento.color = "#E74C3C";
                            }
                            elemento.mesAbreviado = elemento.mes.substring(0, 3) + ".";
                        });
                    }
                },
                error: (error) => {
                    this.redirigeAComponente('pantallaError');
                },
                complete: () => {
                    setString("aReportes", JSON.stringify(this.aReportes));
                }
            });
        this.cancelServicio.push(this.apiSubscribe);
    }

    refrescarReportes(args: ListViewEventData) {
        const listView = args.object;
        setTimeout(() => {
            listView.scrollWithAmount(-listView.getScrollOffset(), true);
            listView.notifyPullToRefreshFinished();
            this.aReportes = false;
            this.recuperaReportes();
        }, 500);
    }

    verMenuLateral(): void {
        if (this.aReportes) {
            setTimeout(() => {
                const sideDrawer = <RadSideDrawer>application.getRootView();
                sideDrawer.showDrawer();
            }, 500);
        }
    }

    nuevoPeriodo() {
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: false,
            cancelable: false,
            stretched: false
        }
        this.modalService.showModal(modalPeriodoComponent, options)
            .then((dialogResult: string) => {
                if (dialogResult) {
                    setString("oPeriodoTiempo", (JSON.stringify(dialogResult)));
                    this.redirigeAComponente('reporte');
                }
            });
    }

    periodoSeleccionado(reporte: any) {
        setString("oPeriodoTiempo", (JSON.stringify(reporte)));
        this.redirigeAComponente('reporte')
    }

    eliminaReporte(reporte: any) {
        dialogs.confirm({
            title: "Reporte de tiempo",
            message: "El reporte de tiempo se eliminará permanentemente del sistema. \n¿Desea continuar?",
            okButtonText: "Si",
            cancelButtonText: "Cancelar",
            cancelable: false
        }).then(result => {
            if (result) {
                this.indicadorProceso.show("Eliminando reporte de tiempo...");
                this.apiSubscribe = this.reportesService.deleteReporteDeTiempo(reporte.id_tiempo)
                    .subscribe({
                        next: () => {

                        },
                        error: () => {
                            this.indicadorProceso.hide();
                            dialogs.alert({
                                title: "Reporte de tiempo",
                                message: "Ocurrio un error al eliminar el reporte de tiempo. Inténtelo mas tarde.",
                                okButtonText: "Aceptar",
                                cancelable: false
                            });
                        },
                        complete: () => {
                            this.indicadorProceso.hide();
                            setTimeout(() => {
                                let index = this.aReportes.indexOf(reporte)
                                this.aReportes.splice(index, 1)
                                if (this.aReportes.length === 0) {
                                    this.aReportes = false;
                                    this.recuperaReportes();
                                }
                            }, 200);
                        }
                    })
                this.cancelServicio.push(this.apiSubscribe);
            }
        });
    }

    reportesPordefault() {
        this.aReportesCargando = [];
        for (let i = 0; i <= 6; i++) {
            let reporte = {};
            this.aReportesCargando.push(reporte);
        }
    }

    redirigeAComponente(componente: string) {
        setTimeout(() => {
            this.indicadorProceso.hide();
            this.zone.run(() => {
                this.routerext.navigate([`./${componente}`], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
            });
        }, 500);
    }
}