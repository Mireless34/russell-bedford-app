import { Component, NgZone, OnDestroy, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";
import { ActivityIndicator, AndroidActivityBackPressedEventData, Utils } from '@nativescript/core';
import { IndicadorProcesos } from "../shared/indicadorProcesos.service";
import { EventData } from '@nativescript/core';
import { Page } from '@nativescript/core';
import { LoginService } from "~/app/shared/login.service";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { AndroidApplication } from '@nativescript/core';
import * as application from '@nativescript/core/application';
import * as appversion from "@nativescript/appversion";
import * as platform from '@nativescript/core/platform';
import * as dialogs from '@nativescript/core/ui/dialogs';
import { exit } from "nativescript-exit";
import { getString, remove, setString } from "@nativescript/core/application-settings";
import { firebase } from "@nativescript/firebase-core";
import { getUUID } from "nativescript-uuid-v2";
import * as EmailValidator from "email-validator";
import { ConfiguraAPI } from "../shared/configuraAPI";

@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
    providers: [LoginService, IndicadorProcesos]
})

export class LoginComponent implements OnInit {

    hayCredenciales = false;
    usuario: string = "";
    contrasenia: string = "";
    versionApp = "";
    versionActualApp = "";
    loginUsuario: any;

    constructor(private routerExtensions: RouterExtensions, private page: Page, private loginService: LoginService,
        private zone: NgZone, private indicadorProceso: IndicadorProcesos, private routerext: RouterExtensions) {
        page.actionBarHidden = true;
        appversion.getVersionName().then((v: string) => {
            this.versionApp = "versión: " + v;
            this.versionActualApp = v;
        });
        if (platform.isAndroid) {
            application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
                data.cancel = true;
                dialogs.confirm({
                    title: "Cerrar Reporte de tiempo",
                    message: "¿Desea cerrar la aplicación?",
                    okButtonText: "Si",
                    cancelButtonText: "No",
                    cancelable: false
                }).then(result => {
                    if (result) {
                        exit();
                    }
                });
            });
        }
    }

    ngOnInit() {
        this.hayCredenciales = true;
        setTimeout(() => {
            const sideDrawer = <RadSideDrawer>application.getRootView();
            sideDrawer.gesturesEnabled = false;
        }, 500);
        this.entrar();
    }

    onBusyChanged(args: EventData) {
        let indicator: ActivityIndicator = <ActivityIndicator>args.object;
    }

    entrar() {
        this.usuario = "";
        this.contrasenia = "";
        if (getString("usuario") && getString("contrasena")) {
            this.usuario = getString("usuario");
            this.contrasenia = getString("contrasena");
            this.login();
        } else {
            this.hayCredenciales = false;
        }
    }

    login() {
        if (this.usuario && this.contrasenia) {
            if (!this.hayCredenciales) {
                this.indicadorProceso.show("Ingresando...");
            }
            this.loginUsuario = null;
            this.loginService.validaAcceso(this.usuario, this.contrasenia)
                .subscribe({
                    next: (data) => {
                        let acceso = JSON.parse(data);
                        if (!acceso.ErrorMessage && acceso.Correct) {
                            acceso.foto = null;
                            setString("loginUsuario", JSON.stringify(acceso.Object));
                            setString("usuario", this.usuario);
                            setString("contrasena", this.contrasenia);
                            this.loginUsuario = acceso.Object;
                        } else {
                            remove("loginUsuario");
                            remove("portrait");
                            this.indicadorProceso.hide();
                            dialogs.confirm({
                                title: "Iniciar sesión",
                                message: acceso.ErrorMessage,
                                okButtonText: "Aceptar",
                                cancelable: false
                            }).then(result => {
                                if (result) {
                                    this.hayCredenciales = false;
                                }
                            });
                        }
                    },
                    error: (error) => {
                        this.indicadorProceso.hide();
                        if (error.status === 401) {
                            dialogs.confirm({
                                title: "Iniciar sesión",
                                message: error.error.ErrorMessage,
                                okButtonText: "Aceptar",
                                cancelable: false
                            }).then(result => {
                                if (result) {
                                    this.hayCredenciales = false;
                                }
                            });
                        } else {
                            this.redirigeAComponente('pantallaError');
                        }
                    },
                    complete: () => {
                        if (this.loginUsuario) {
                           this.registraDispositivo();
                           this.ingresar();
                        }
                    }
                });
        } else {
            this.hayCredenciales = false;
            dialogs.alert({
                title: "Iniciar sesión",
                message: "Indique usuario o contraseña",
                okButtonText: "Aceptar",
                cancelable: false
            });
        }
    }

    registraDispositivo() {
        let idDevice = platform.isIOS ? getUUID() : platform.device.uuid;
        firebase().messaging().registerDeviceForRemoteMessages()
            .then(() => {
                firebase().messaging().getToken().then((token: string) => {
                    // may be null if not known yet
                    console.log("Current push token: " + token);
                    if (token) {
                        this.loginService.setTokenDispositivoNotificacionesPush(token, idDevice, this.loginUsuario.id_empleado, platform.device.os, platform.device.osVersion, platform.device.manufacturer)
                            .subscribe({
                                next: (data) => {
                                    let setTokenDevice = JSON.parse(data);
                                    if (setTokenDevice) {
                                        //se guarda informacion
                                    }
                                },
                                error: (error) => {
                                },
                                complete: () => {
                                }
                            });
                    } else {
                    }
                });
            })
            .catch((e) => {
                setTimeout(() => {
                    this.indicadorProceso.hide();
                }, 1000);
            });
    }

    ingresar() {
        // if (!this.hayCredenciales) {
        this.redirigeAComponente('reportes');
        // }
    }

    recuperarContrasena() {
        dialogs.prompt({
            title: "¿Tiene problemas para iniciar sesión?",
            message: "Ingrese su correo electrónico que le proporciono la empresa.",
            inputType: dialogs.inputType.email,
            defaultText: "",
            cancelable: false,
            okButtonText: "Recuperar contraseña",
            cancelButtonText: "Cancelar",
        }).then((data) => {
            if (data.result) {
                if (data.text) {
                   this.comprobarEmail(data.text);
                } else {
                    dialogs.alert({
                        title: 'Iniciar sesión',
                        message: 'Correo electrónico no valido.',
                        okButtonText: 'Aceptar'
                    });
                }
            }
        });
    }

    comprobarEmail(correoElectronico: string) {
        if (correoElectronico && correoElectronico !== "") {
            if (!EmailValidator.validate(correoElectronico)) {
                dialogs.alert({
                    title: 'Iniciar sesión',
                    message: 'La dirección de correo electrónico no es valida, verifique e inténtelo nuevamente.',
                    okButtonText: 'Aceptar'
                });
                return;
            }
        }
        this.indicadorProceso.show("Validando correo eletrónico...");
        this.loginService.validaEmail(correoElectronico)
            .subscribe({
                next: (data) => {
                    let eMail = JSON.parse(data);
                    if (eMail.Correct) {
                        this.enviarEmail(correoElectronico);
                    } else {
                        dialogs.alert({
                            title: 'Reporte de tiempo',
                            message: 'Esta dirección de correo electrónico no está registrada en el sistema.\n\nPor favor asegúrese de capturar la dirección de correo electrónico que la empresa le proporciono.',
                            okButtonText: 'Aceptar',
                            cancelable: false
                        })
                    }
                },
                error: (error) => {
                    this.indicadorProceso.hide();
                    if (error.status === 404) {
                        dialogs.alert({
                            title: 'Reporte de tiempo',
                            message: 'Esta dirección de correo electrónico no está registrada en el sistema.\n\nPor favor asegúrese de capturar la dirección de correo electrónico que la empresa le proporciono.',
                            okButtonText: 'Aceptar',
                            cancelable: false
                        })
                    } else {
                        dialogs.alert({
                            title: 'Iniciar sesión',
                            message: 'Error al validar el correo eletrónico, inténtelo nuevamente.',
                            okButtonText: 'Aceptar',
                            cancelable: false
                        });
                    }
                },
                complete: () => {
                    this.indicadorProceso.hide();
                }
            });
    }

    enviarEmail(email: string) {
        this.indicadorProceso.show("Enviando correo eletrónico...");
        this.loginService.enviarCorreoElectronico(email,'','','','','')
            .subscribe({
                next: (data) => {
                    let eMail = JSON.parse(data);
                    if (eMail.Correct) {
                        dialogs.confirm({
                            title: 'Reporte de tiempo',
                            message: `Sigue las instrucciones que te hemos enviado a ${email}`,
                            okButtonText: 'Aceptar',
                            cancelable: false
                            
                        });
                    } else {
                        dialogs.alert({
                            title: 'Reporte de tiempo',
                            message: 'Esta dirección de correo electrónico no está registrada en el sistema.\n\nPor favor asegúrate de capturar la dirección de correo electrónico que la empresa te proporciono.',
                            okButtonText: 'Aceptar',
                            cancelable: false
                        });
                    }
                }, error: (error) => {
                    this.indicadorProceso.hide();
                    dialogs.alert({
                        title: 'Iniciar sesión',
                        message: 'No se pudo validar el correo eletrónico, inténtelo nuevamente.',
                        okButtonText: 'Aceptar'
                    });
                }, complete: () => {
                    this.indicadorProceso.hide();
                }
            });
    }

    verAvisoDePrivacidad() {
        Utils.openUrl(encodeURI("https://www.russellbedford.mx/sp/aviso-de-privacidad/" + "?n=" + Math.random()));
    }

    redirigeAComponente(componente: string) {
        setTimeout(() => {
            this.indicadorProceso.hide();
            this.zone.run(() => {
                this.routerext.navigate([`./${componente}`], { clearHistory: true, animated: true, transition: { name: "slideRight" } })
            });
        }, 500);
    }

    configuraIP() {
        dialogs.prompt({
          title: 'Contraseña',
          inputType: dialogs.inputType.password,
          message: 'Ingrese la contraseña',
          cancelable: false,
          okButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar',
        }).then((data) => {
          if (data.result) {
            if ((<any>global).btoa(data.text) === 'U2lzdGVtYXMjMjAyMw==') {
              dialogs.prompt({
                title: 'Configuración de IP',
                inputType: dialogs.inputType.phone,
                message: 'Configura la dirección IP para completar la conexión',
                cancelable: false,
                okButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                defaultText: getString('DireccionIP')
              }).then((data) => {
                if (data.result) {
                  if (data.text) {
                    setString('DireccionIP', (data.text));
                    ConfiguraAPI.API_URL = `http://${data.text}/stuxnet/api/`;
                  }
                }
              });
            } else {
              dialogs.alert({
                title:"Contraseña",
                message: "La contraseña es incorrecta, favor de intentarlo nuevamente",
                okButtonText: 'Aceptar',
                cancelable: false
              })
            }
          }
        });
      }
}