// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { runNativeScriptAngularApp, platformNativeScript } from "@nativescript/angular";
import { enableProdMode } from '@angular/core';
import { AppModule } from "./app/app.module";
import { firebase } from '@nativescript/firebase-core';
import '@nativescript/firebase-messaging'
import '@nativescript/firebase-analytics'

firebase()
  .initializeApp()
  .then((done) => {
    console.log('initializeApp', done);
    firebase().messaging()
      .requestPermission({ ios: { alert: true, badge: true, sound: true } })
      .then(() => {
        console.log('permissions done');
        firebase().messaging().registerDeviceForRemoteMessages();
      });
  });
  
enableProdMode();
runNativeScriptAngularApp({
    appModuleBootstrap: () => platformNativeScript().bootstrapModule(AppModule),
  });
